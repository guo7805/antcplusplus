#ifndef ANT_H
#define ANT_H

#include "OpenGLSupport/GlutDemoApplication.h"
#include "btBulletDynamicsCommon.h"
#include <iostream>
#include <fstream>
#include <vector>
#include "neuron.h"
#include "muscle.h"
#include "tarsus.h"


#define NUM_LEGS 6
#define NUM_BODYPARTS NUM_LEGS*3+1
#define NUM_JOINT NUM_LEGS*3




class ant
{
    #ifdef OPTIMIZATION
    int                 m_indGen;
    int                 m_indX;
    #endif
    btScalar            m_fCyclePeriod;
    btDynamicsWorld*	m_ownerWorld;
    btCollisionShape*	m_shapes[NUM_BODYPARTS];
    btRigidBody*		m_bodies[NUM_BODYPARTS];
    btHingeConstraint*	m_joints[NUM_JOINT];
    btScalar            m_bodylength[NUM_BODYPARTS];
    btScalar            m_bodyradius[NUM_BODYPARTS];
    btScalar            m_bodymass[NUM_BODYPARTS];
    btScalar            m_maxAngle[NUM_JOINT];
    btScalar            m_minAngle[NUM_JOINT];
    btScalar            m_angleVel[NUM_JOINT];
    btScalar            m_cpgFreq[NUM_JOINT];
    btScalar            m_cpgPhase[NUM_JOINT];
    btScalar            m_cpgAmp[NUM_JOINT];
    btScalar            m_cpgOffset[NUM_JOINT];
    btRigidBody*        head_body;
    btRigidBody*        abdomen_body;
    btPoint2PointConstraint* m_footContact[NUM_LEGS];
    btScalar            m_footContact_ReleaseTime[NUM_LEGS];
    bool                m_footStatus[NUM_LEGS]; //keep track of foot status. if true, standing, if false, swinging
    btScalar            m_percent[NUM_LEGS];


    muscle*             m_muscles[NUM_JOINT*2];
    neuron*             m_neurons[NUM_JOINT*2];
    tarsus*             tarsusPointer;
    static const short COL_BODY = 1<<(0);
    static const short bodyCollidesWith = 1<<(1);


    vector<btVector3>   positionHistory;
    vector<btVector3>   rotationHistory;
    vector< vector<btVector3> > angleHistory;
    vector< vector<bool> > contactHistory;

    btRigidBody* localCreateRigidBody (btScalar mass, const btTransform& startTransform, btCollisionShape* shape, short group, short mask);

public:
    ant(btDynamicsWorld* ownerWorld);
    ant(btDynamicsWorld* ownerWorld, int indGen, int indX);
    virtual ~ant () {
        delete [] m_shapes;
        delete [] m_bodies;
        delete [] m_joints;
        delete [] m_bodylength;
        delete [] m_bodyradius;
        delete [] m_bodymass;
        delete [] m_maxAngle;
        delete [] m_minAngle;
        delete [] m_angleVel;
        delete [] m_cpgFreq;
        delete [] m_cpgPhase;
        delete [] m_cpgAmp;
        delete [] m_cpgOffset;
        delete [] m_footContact;
        delete [] m_footContact_ReleaseTime;
        delete [] m_footStatus;
        delete [] m_muscles;
        delete [] m_neurons;
        delete [] m_percent;

        delete head_body;
        delete abdomen_body;
        delete tarsusPointer;
    };
    //btMatrix3x3 jacobian(bool isLinear, btScalar alpha, btScalar beta, btScalar gamma);
    btTransform getConstraintFrame(int indexJoint);

    void applyTorqueOnJoint(btHingeConstraint* joint, btScalar torque);
    void driveJoint(btScalar fTargetPercent, btScalar timeStep);
    void driveJointByMotor(btScalar m_Time, btScalar timeStep);

    void readParam(btScalar m_bodylength[NUM_BODYPARTS], btScalar m_bodyradius[NUM_BODYPARTS], btScalar m_maxAngle[NUM_JOINT], btScalar m_minAngle[NUM_JOINT]);
    void readCpgParam(btScalar m_cpgFreq[], btScalar m_cpgPhase[], btScalar m_cpgAmp[], btScalar m_cpgOffset[]);
    void writeParam(btScalar maxAngle[NUM_BODYPARTS], btScalar minAngle[NUM_BODYPARTS]);
    void keepRecord(bool position=false, bool rotation=false, bool angles=false, bool contact=false);
    vector<btVector3> readAllJointAngles();
    void checkFootContact(btScalar m_Time);
    void checkFootRelease(btScalar m_Time);
    int checkWhichLeg(btRigidBody* tibia);
    btScalar checkTiming(btScalar m_Time, int indLeg);
    btScalar checkNeighbor(int indLeg);
    btScalar checkExtremePose(int indLeg, btScalar currentTime, btScalar contactTime);
    int getWalkingDirection();
    bool checkIsFootEnd(btVector3 localPos);
    btVector3 getFootEndInGlobal(int indLeg);
    void resetTPose();
    void updateTargetAngle(int indLeg);

    void getDOFsOneLeg(int legIndex, btScalar DOFs[3]);
    void readAllFootStatus(vector<bool>& contact_per_frame);
    btHingeConstraint** getJoints();
    neuron** getNeurons();
    muscle** getMuscles();
    btRigidBody** getBodies();
    btVector3 getCOMposition();

    void setupGeometry();
    void setupNeuronMuscle();
    void setAngleLimits(btScalar max_Angle[], btScalar min_Angle[]);


    int getNumDOFs() { return NUM_JOINT+1;};
    btScalar getCyclePeriod() {return m_fCyclePeriod;};
//    void updateOscParam(int index, btScalar delta);
    // void recordVector(ofstream record, btVector3 vec);
    void drawDebugFrame();

};

#endif
