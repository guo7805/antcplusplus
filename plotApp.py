from Tkinter import *
from plotTools import *
import numpy as np

MODES = [
    ("COM Translation", "0"),    
    ("COM Rotation", "1"),
    ("Joint Rotation", "2"),
    ("Foot Status", "3"),
]

class plotApp:
    pt = plotTools()
    
    
    def __init__(self, master):        
        dataTypeFrame = Frame(master)
        dataTypeFrame.pack(side=LEFT)
        self.dataType = IntVar()
        for text, val in MODES:
            b = Radiobutton(dataTypeFrame, text=text, variable=self.dataType, value=val, command=self.loadData)
            b.pack(anchor=W)
        
        rowFrame = Frame(master)
        rowFrame.pack(side=RIGHT)
        row1 = Frame(rowFrame)
        row1.pack(side=TOP)
        L1 = Label(row1, text="Starting Row")   
        L1.pack( side = LEFT)
        self.startRow = IntVar()
        E1 = Entry(row1, textvariable=self.startRow)
        E1.pack( side = RIGHT)
        
        row2 = Frame(rowFrame)
        row2.pack(side=TOP)
        self.endRow = IntVar()
        L2 = Label(row2, text="Ending Row")        
        L2.pack( side = LEFT)
        E2 = Entry(row2, textvariable=self.endRow)    
        E2.pack( side = RIGHT)      
   
        row3 = Frame(rowFrame)
        row3.pack(side=TOP)        
        self.selectedCols = StringVar()
        self.numCols = StringVar()
        self.numCols.set('Selected Columns')
        L3 = Label(row3, textvariable=self.numCols)        
        L3.pack( side = LEFT)
        E3 = Entry(row3, textvariable=self.selectedCols)    
        E3.pack( side = RIGHT)          
        
        row4 = Frame(rowFrame)
        row4.pack(side=TOP)        
        self.isSubfig = IntVar()
        subFigCheck = Checkbutton(row4, text="Draw in same figure?", variable=self.isSubfig)
        subFigCheck.pack(side=LEFT)
            
        self.button = Button(master, text="PLOT", command = self.plot)
        self.button.pack(side=BOTTOM)        
    #end
    
    def loadData(self):
        self.dataInput = self.pt.readData(self.dataType.get())
        rows, cols = self.dataInput.shape        
        self.endRow.set(rows)
        self.numCols.set('Selected Columns out of ' + str(cols))
        # numCols = self.plotData
    #end
    
    def fastTrack(self):
        pass
    #end
        
    def plot(self):
        startRow = self.startRow.get()
        endRow = self.endRow.get()
        colsStr = self.selectedCols.get()
        colsInt = [int(n) for n in colsStr.split(',')]
        legends = self.pt.getLegend(self.dataType.get(), colsInt)
        # if self.isSubfig
        self.pt.plot(self.dataInput[startRow:endRow, colsInt], legends, self.isSubfig.get())
    #end



win = Tk()  
win.title('Plotting Application For Insect Simulation')  
win.geometry('600x200')    
app = plotApp(win)
win.mainloop()