#include "muscle.h"
#include "btBulletDynamicsCommon.h"
#include <stdio.h>
#include<cmath>
#include <iomanip>
using namespace std;



muscle::muscle(int ind, btScalar limbLen, btScalar limbRad, btScalar maxAngle, btScalar minAngle, btScalar mF)
{
    maxForce = mF;
    maxVel = 0.1;

    index = ind;

    staticLen = 0.8*limbLen;

    minLen = 0.5*staticLen;
    maxLen = 1.5*staticLen;

    // Even index is the extensor
    // Odd Index is the flexor
    if (ind%2 == 0)
    {
        len = maxLen;
        vel = -maxVel;
    }
    else
    {
        len = minLen;
        vel = maxVel;
    }

    activation = 0;


    staticAngle = (maxAngle + minAngle)/2;
    // preAngle = staticAngle;
    // preAngle = 0.0;
    // flexor: 0.0006m; extensor: 0.0003m
    // levelArm = 0.25*limbRad;
    levelArm = staticLen/2/sin((maxAngle - minAngle)/2);
    //ctor


    passiveK = 0.0095;
    passiveAlpha = 2.0;

    fl_a0 = 0.00091;
    fl_a1 = -0.41618;
    fl_a2 = 1.42961;
    fl_a3 = -1.21435;
    fl_a4 = 0.3;

    fv_b = 0.52;
    fv_f = 1.5;

    char filename[10];
    sprintf(filename, "record/muscle/%d.txt", index);
    record.open(filename);
}

muscle::~muscle()
{
    //dtor
    record.close();
}

btScalar muscle::getStaticLen()
{
    return staticLen;
}

void muscle::setVel(btScalar mvel)
{
    vel = mvel;
}

void muscle::setLen(btScalar mlen)
{
    len = mlen;
}

btScalar muscle::getVel()
{
    return vel;
}

btScalar muscle::getLen()
{
    return len;
}

void muscle::activationDynamics(bool excited, btScalar excitation)
{
    btScalar normExc = (excitation + 65)/95;
    btScalar dac;
    if (excited)
    {
        dac = (normExc - (0.01 + 0.99*normExc)*activation)/10;
    }
    else
    {
        dac = -activation/50;
    }

    activation = activation + dac;
}

btScalar muscle::normalizeAngle(btScalar angle)
{
    // Extensor makes the angle greater
    // Flexor makes the angle smaller
    // substract the static pose from this
    // btScalar newangle;

    if (index%2 == 0)
    {
        return -abs(angle);
    }
    else
    {
       return abs(angle);
    }
    // return newangle;
}

void muscle::updateLenVel(btScalar angle, btScalar deltaT)
{
    //btScalar newangle = this->normalizeAngle(angle);
    btScalar deltaLen;


    if (index%2 == 0)   // Even index is the extensor
    {

        /*
        if (angle > 0)
        {
            deltaLen = -sin(angle);
        }
        else
        {
            deltaLen = -sin(angle);
        }
        */
        deltaLen = -levelArm*sin(angle - staticAngle);
    }
    else            // Odd index is the flexor
    {
        /*
        if (angle > 0)
        {
            deltaLen = sin(angle);
        }
        else
        {
            deltaLen = sin(angle);
        }
        */
        deltaLen = levelArm*sin(angle - staticAngle);
    }


    vel = (staticLen + deltaLen - len) / deltaT;
    if (vel > maxVel)
    {
        vel = maxVel;
    }
    if (vel < -maxVel)
    {
        vel = -maxVel;
    }
    len = staticLen + deltaLen;


    //return len;
}

btScalar muscle::getTorque(bool excited, btScalar excitation, btScalar angle, btScalar deltaT)
{
    this->activationDynamics(excited, excitation);
    this->updateLenVel(angle, deltaT);

    // record << fixed << setprecision(10) << angle << " " << len << " " << vel << " ";
    // record << excitation << " " << activation << " " ;

    btScalar force = getForce();
    btScalar torque = force*levelArm;

    //record << torque << endl;

    return torque;
}

btScalar muscle::getForce()
{
    btScalar activeF = activeForce();
    btScalar passiveF = passiveForce();

    // record << activeF << " " << passiveF << " ";

    return activeF + passiveF;
}

btScalar muscle::activeForce()
{
    btScalar fv = forcevelocity();
    btScalar fl = forcelength();
    //record << fl << " ";
    return maxForce*activation*fv*fl;
}

btScalar muscle::forcevelocity()
{
    btScalar force = -vel/maxVel + 1;

    return force;
    /*
    if (nonDim_v < 0)
    {
        // shortening
        return (1 - nonDim_v)/(1 + nonDim_v/fv_b);

    }
    else
    {
        // lenghtening
        btScalar temp = (1 + fv_b)/(fv_f - 1)*nonDim_v;
        return (1 - fv_f*temp)/(1 - temp);
    }
    */
}

btScalar muscle::forcelength()
{
    // update the length first!!!

    if ((len > maxLen) || (len < minLen))
    {
        return btScalar(0.0);
    }
    else
    {
        btScalar nonDim_Len = len/staticLen;
        //eturn -4*(nonDim_Len - 0.5)*(nonDim_Len - 1.5);
        return fl_a0 + fl_a1*nonDim_Len + fl_a2*pow(nonDim_Len, 2.0) + fl_a3*pow(nonDim_Len, 3.0) + fl_a4*pow(nonDim_Len, 4.0);

    }
}


btScalar muscle::passiveForce()
{

    if (len < staticLen)
    {
        return btScalar(0.0);
    }
    else
    {
        return passiveK*pow((len/staticLen - 1), passiveAlpha);
    }
}

void muscle::testFL()
{
    btScalar curLen;
    curLen = minLen;
    btScalar step = (maxLen - minLen)/100;
    do
    {
        len = curLen;
        btScalar force = this->forcelength();
        //record << curLen << " " << force << endl;
        curLen = curLen + step;
    }while(curLen < maxLen);
}

void muscle::testFV()
{
    btScalar curVel;
    curVel = - maxVel;
    btScalar step = maxVel/100;
     do
     {
         vel = curVel;
         btScalar force = this->forcevelocity();
         //record << curVel << " " << force << endl;
         curVel = curVel + step;
     }while(curVel < maxVel);
}
