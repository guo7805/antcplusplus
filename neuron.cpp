#include "neuron.h"
#include "btBulletDynamicsCommon.h"
#include <stdio.h>
#include <cmath>
using namespace std;




neuron::neuron(int ind, btScalar df)
{
    //ctor
    index = ind;

    dutyfactor = df;

    // parameters are set following "Simple Model of Spiking Neurons" by Eugene Izhikevich
    params[0] = 0.02;
    params[1] = 0.2;
    params[2] = -65;
    params[3] = 2;

    v = -65;
    //u = params[1]*v;
    u = -5.5;
    char filename[10];
    sprintf(filename, "record/neuron/%d.txt", index);
    //record.open("record/neuron.txt");
    record.open(filename);
}

neuron::~neuron()
{
    //dtor
    record.close();
}

btScalar neuron::getu()
{
    return u;
}

btScalar neuron::getv()
{
    record << v << " "  << u << endl;
    return v;
}

btScalar neuron::restS()
{
    v = -65;
    u = -5.5;
    return v;
}

btScalar neuron::activeS(btScalar deltaT, btScalar percent)
{
    // convert deltaT to milliseconds
    btScalar stim;
    if (percent < dutyfactor)
    {
        stim = 10;
    }
    else
    {
        stim = 0;
    }
    btScalar dv = 0.04*v*v + 5*v + 140 - u + stim;
    btScalar du = params[0]*(params[1]*v - u);

    v = v + dv;
    u = u + du;

    if (v >= 30)
    {
        v = params[2];
        u = u + params[3];
    }
    return v;
}
