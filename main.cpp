#include "swarmDemo.h"
#include "OpenGLSupport/GlutStuff.h"
#include "OpenGLSupport/GLDebugDrawer.h"

#include "btBulletDynamicsCommon.h"

#define NUM_PARAM_PER_ARRAY 18
int main(int argc,char** argv)
{



    swarmDemo* crowdSim = new swarmDemo();

//    btVector3 pos = crowdSim->getCameraPosition();
//    btVector3 target = crowdSim->getCameraTargetPosition();
//
//

//    ant** a = crowdSim->getPlayers();
//    muscle** m = a[0]->getMuscles();
    // Be sure to turn this off because it will mess up your velocity and length settings
    //m[0]->testFV();
    //m[0]->testFL();
#ifdef OPTIMIZATION
    //std::cout << "running background" << endl;
    crowdSim->initPhysics();
    crowdSim->initCharacters_Opt(atoi(argv[1]), atoi(argv[2]));
    crowdSim->runSimulationBackground();
    return 0;
#elif defined FLUT
    crowdSim->initPhysics();
    crowdSim->initCharacters();
    crowdSim->runSimulationBackground();
    return 0;
#else
    crowdSim->initPhysics();
    crowdSim->initCharacters();
    crowdSim->setDebugMode(btIDebugDraw::DBG_DrawConstraints+btIDebugDraw::DBG_DrawConstraintLimits);
    return glutmain(argc, argv,640,480,"Ant Simulation by Shihui Guo. http://www.guoshihui.net", crowdSim);
#endif

}

