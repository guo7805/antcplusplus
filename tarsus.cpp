#include "tarsus.h"

tarsus::tarsus()
{
    eta_viscous = 1.0;
    gamma_adhesion = 1.0;
    theta_adhesion = M_PI/6;
    //ctor
}

tarsus::~tarsus()
{
    //dtor
}

tarsus::tarsus(const tarsus& other)
{
    //copy ctor
}

tarsus& tarsus::operator=(const tarsus& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}

btScalar tarsus::frictionForce(btScalar velocity, btScalar radius, btScalar fluidHeight)
{
    btScalar contactArea = M_PI*radius*radius;
    btScalar fri_force = eta_viscous*contactArea*velocity/fluidHeight;
    return fri_force;
}

btScalar tarsus::adhesionViscous(btScalar radius, btScalar fluidHeight, btScalar fluidHeightVelocity)
{
    btScalar viscousForce = fluidHeightVelocity*(3*M_PI*eta_viscous/2/fluidHeight/fluidHeight/fluidHeight)*radius*radius*radius*radius;
    return viscousForce;
}

btScalar tarsus::adhesionTension(btScalar radius)
{
    btScalar tension =2*M_PI*gamma_adhesion*radius;
    return tension;
}

btScalar tarsus::adhesionPressure(btScalar radius, btScalar fluidHeight)
{
    btScalar pressure = M_PI*gamma_adhesion*radius*radius*(2*cos(theta_adhesion)/fluidHeight - 1/radius);
    return pressure;
}
