#ifndef SWARMDEMO_H
#define SWARMDEMO_H

//#include "GlutDemoApplication.h"
#include "ant.h"
#include "LinearMath/btAlignedObjectArray.h"



class btBroadphaseInterface;
class btCollisionShape;
class btOverlappingPairCache;
class btCollisionDispatcher;
class btConstraintSolver;
struct btCollisionAlgorithmCreateFunc;
class btDefaultCollisionConfiguration;

class swarmDemo : public GlutDemoApplication
{
    btScalar        m_Time;

    btScalar        timeStep;
    btVector3       m_camInObject;
    btRigidBody*    ground;

    bool     recorded;
    static const short COL_GROUND = 1<<(1);
    static const short groundCollidesWith = 1<<(0);


    btAlignedObjectArray<class ant*> ant_players;
    btAlignedObjectArray<btCollisionShape*>	ant_collisionShapes;
    btBroadphaseInterface*	ant_broadphase;

    btCollisionDispatcher*	ant_dispatcher;

    btConstraintSolver*	ant_solver;

    btDefaultCollisionConfiguration* ant_collisionConfiguration;



public:
    void initPhysics();

    void exitPhysics();

    ~swarmDemo()
    {
        exitPhysics();
    }

    virtual void clientMoveAndDisplay();

    virtual void displayCallback();

    virtual void keyboardCallback(unsigned char key, int x, int y);

    static DemoApplication* Create()
    {
        swarmDemo* crowdSim = new swarmDemo();
        crowdSim->myinit();
        crowdSim->initPhysics();
        return crowdSim;
    }

    btRigidBody*	localCreateRigidBody(float mass, const btTransform& startTransform,btCollisionShape* shape, short group, short mask);

    void setMotorTargets();

    void runSimulationBackground();

    // use this function only to set the parameters in the case of background optimization
    void setAgentAngleLimits(btScalar max_Angle[], btScalar min_Angle[]);

    void resetCamera();

    void initCharacters();
    void initCharacters_Opt(int indGen, int indX);
    ant** getPlayers();


};

struct footContactResultCallback : public btCollisionWorld::ContactResultCallback
{
	virtual	btScalar	addSingleResult(btManifoldPoint& cp,
                                  const btCollisionObjectWrapper* colObj0Wrap, int partId0,int index0,
                                  const btCollisionObjectWrapper* colObj1Wrap,int partId1,int index1)
	{
        cout << "foot contact" << endl;
		glBegin(GL_LINES);
		glColor3f(0, 0, 0);

		btVector3 ptA = cp.getPositionWorldOnA();
		btVector3 ptB = cp.getPositionWorldOnB();

		glVertex3d(ptA.x(),ptA.y(),ptA.z());
		glVertex3d(ptB.x(),ptB.y(),ptB.z());
		glEnd();

		//btTransform tr_a = colObj0Wrap.getWorldTransform();
		//btTransform tr_b = colObj1Wrap.getWorldTransform();

		//btCollisionObject* rb_a = colObj0Wrap.getCollisionObject();
		//btCollisionObject* rb_b = colObj1Wrap.getCollisionObject();

		btVector3 local_a = cp.m_localPointA;
		btVector3 local_b = cp.m_localPointB;

		// btPoint2PointConstraint* p2pConstraint;
        //p2pConstraint = new btPoint2PointConstraint(rb_a, rb_b, local_a, local_b);

		//return p2pConstraint;
		return 0;
	}
};
#endif
