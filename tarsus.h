#ifndef TARSUS_H
#define TARSUS_H
#include "btBulletDynamicsCommon.h"


class tarsus
{
    private:
        btScalar eta_viscous;
        btScalar gamma_adhesion;
        btScalar theta_adhesion;
    public:
        tarsus();
        virtual ~tarsus();
        tarsus(const tarsus& other);
        tarsus& operator=(const tarsus& other);
        btScalar frictionForce(btScalar velocity, btScalar radius, btScalar fluidHeight);
        btScalar adhesionViscous(btScalar radius, btScalar fluidHeight, btScalar fluidHeightVelocity);
        btScalar adhesionTension(btScalar radius);
        btScalar adhesionPressure(btScalar radius, btScalar fluidHeight);
    protected:
};

#endif // TARSUS_H
