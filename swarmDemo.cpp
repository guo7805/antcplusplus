#include "btBulletDynamicsCommon.h"
#include "swarmDemo.h"
#include "OpenGLSupport/GlutStuff.h"
#include "OpenGLSupport/GL_ShapeDrawer.h"

#include "LinearMath/btIDebugDraw.h"

#include "OpenGLSupport/GLDebugDrawer.h"

#include <iostream>
#include <fstream>
using std::ofstream;
using std::endl;
using std::cerr;

void swarmDemo::resetCamera()
{
    m_azi = 215;
    m_ele = 45;
    setCameraDistance(btScalar(7.5));
    m_camInObject = getCameraPosition();
}

void motorPreTickCallback (btDynamicsWorld *world, btScalar timeStep)
{
    swarmDemo* crowdSim = (swarmDemo*)world->getWorldUserInfo();

    crowdSim->setMotorTargets();

}



void swarmDemo::initPhysics()
{
    m_Time = 0;

    timeStep = 0.001;
    recorded = false;
    setTexturing(true);
    setShadows(true);

    // Setup the basic world



    ant_collisionConfiguration = new btDefaultCollisionConfiguration();

    ant_dispatcher = new btCollisionDispatcher(ant_collisionConfiguration);

    btVector3 worldAabbMin(-10000,-10000,-10000);
    btVector3 worldAabbMax(10000,10000,10000);
    ant_broadphase = new btAxisSweep3 (worldAabbMin, worldAabbMax);

    ant_solver = new btSequentialImpulseConstraintSolver;


    m_dynamicsWorld = new btDiscreteDynamicsWorld(ant_dispatcher, ant_broadphase, ant_solver, ant_collisionConfiguration);

    m_dynamicsWorld->setInternalTickCallback(motorPreTickCallback,this,true);

//  btContactSolverInfo solverInfo = theDynamicsWorld->getSolverInfo();
//  solverInfo.m_erp = 0.8;
//  solverInfo.m_globalCfm = 0.1;



    // Setup a big ground box
    //btRigidBody* ground;
    {
        btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(200.),btScalar(10.),btScalar(200.)));
        ant_collisionShapes.push_back(groundShape);
        btTransform groundTransform;
        groundTransform.setIdentity();
        groundTransform.setOrigin(btVector3(0,-10,0));
        ground = localCreateRigidBody(btScalar(0.),groundTransform,groundShape, COL_GROUND, groundCollidesWith);
        //ground->setFriction(0.5f);
    }

    btContactSolverInfo &solverInfo = m_dynamicsWorld->getSolverInfo();
    solverInfo.m_erp = 0.8;


    resetCamera();
    clientResetScene();
}

void swarmDemo::initCharacters()
{
    ant* agent = new ant(m_dynamicsWorld);
    ant_players.push_back(agent);
}

void swarmDemo::initCharacters_Opt(int indGen=0, int indX = 0)
{
    ant* agent = new ant(m_dynamicsWorld, indGen, indX);
    ant_players.push_back(agent);
}

btRigidBody*	swarmDemo::localCreateRigidBody(float mass, const btTransform& startTransform,btCollisionShape* shape, short group, short mask)
{
	btAssert((!shape || shape->getShapeType() != INVALID_SHAPE_PROXYTYPE));

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0,0,0);
	if (isDynamic)
		shape->calculateLocalInertia(mass,localInertia);

	//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects

#define USE_MOTIONSTATE 1
#ifdef USE_MOTIONSTATE
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);

	btRigidBody::btRigidBodyConstructionInfo cInfo(mass,myMotionState,shape,localInertia);

	btRigidBody* body = new btRigidBody(cInfo);
	body->setContactProcessingThreshold(m_defaultContactProcessingThreshold);

#else
	btRigidBody* body = new btRigidBody(mass,0,shape,localInertia);
	body->setWorldTransform(startTransform);
#endif//

	m_dynamicsWorld->addRigidBody(body, group, mask);

	return body;
}

void swarmDemo::setMotorTargets()
{
    // Unit: deltaTime: seconds
    // Unit: ms: microseconds

    float ms = timeStep*1000000.;
    float minFPS = 1000000.f/60.f;
    if (ms > minFPS)
        ms = minFPS;
    m_Time += ms;


    //footContactResultCallback ft_renderCallback;

    for (int r=0; r<ant_players.size(); r++)
    {

        // ant_players[r]->driveJoint(fTargetPercent, timeStep);
        ant_players[r]->driveJointByMotor(m_Time, timeStep);
        //btRigidBody** agent_bodies = ant_players[r]->getBodies();
        //for (int indLeg = 0; indLeg < NUM_LEGS; indLeg++)
        //{
        //    m_dynamicsWorld->contactPairTest(ground, agent_bodies[3+3*indLeg], ft_renderCallback);
        //}
        ant_players[r]->checkFootContact(m_Time);
        ant_players[r]->checkFootRelease(m_Time);

    }
    btVector3 comPos = ant_players[0]->getCOMposition();
    m_cameraTargetPosition = btVector3(comPos[0], 0.0, comPos[2]);
}


void swarmDemo::clientMoveAndDisplay()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //simple dynamics world doesn't handle fixed-time-stepping
    float deltaTime = getDeltaTimeMicroseconds()/1000000.f;
    //btScalar deltaTime = 0.001;
    int maxSubSteps = 20;

    if (m_dynamicsWorld)
    {
        m_dynamicsWorld->stepSimulation(deltaTime, maxSubSteps, timeStep);
        m_dynamicsWorld->debugDrawWorld();
    }
    renderme();

    glFlush();

    glutSwapBuffers();

}

void swarmDemo::displayCallback()
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (m_dynamicsWorld)
    {
        m_dynamicsWorld->debugDrawWorld();
    }

    renderme();

    glFlush();
    glutSwapBuffers();

}

void swarmDemo::runSimulationBackground()
{
    float deltaTime = 0.016;
    #ifdef OPTIMIZATION
    float totalT = 20; // unit: seconds
    #elif defined FLUT
    float totalT = 2; // unit: seconds
    #endif // OPTIMIZATION
    int maxSubSteps = 20;
    do
    {
        if (m_dynamicsWorld)
        {
            m_dynamicsWorld->stepSimulation(deltaTime, maxSubSteps, timeStep);
        }
        totalT = totalT - deltaTime;
    }
    while (totalT > 0);

    for (int r=0; r<ant_players.size(); r++)
    {
        ant_players[r]->keepRecord(true, true, true);
    }
}

void swarmDemo::setAgentAngleLimits(btScalar max_Angle[], btScalar min_Angle[])
{
    if (ant_players.size() == 1)
    {
        ant_players[0]->setAngleLimits(max_Angle, min_Angle);
    }
}


void swarmDemo::keyboardCallback(unsigned char key, int x, int y)
{

    switch (key)
    {

    case 'q':
        for (int r=0; r<ant_players.size(); r++)
        {
            ant_players[r]->keepRecord(true, true, true, true);
        }


    default:
        DemoApplication::keyboardCallback(key, x, y);
    }
}

void swarmDemo::exitPhysics()
{
    int i;

    for (i=0; i<ant_players.size(); i++)
    {
        ant* agent = ant_players[i];
        cout << "agent destructed" << endl;
        agent->keepRecord(true, true, true);
        delete agent;
    }


    //remove the rigidbodies from the dynamics world and delete them

    for (i=m_dynamicsWorld->getNumCollisionObjects()-1; i>=0 ; i--)
    {
        btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
        btRigidBody* body = btRigidBody::upcast(obj);
        if (body && body->getMotionState())
        {
            delete body->getMotionState();
        }
        m_dynamicsWorld->removeCollisionObject( obj );
        delete obj;
    }


    //delete collision shapes
    for (int j=0; j<ant_collisionShapes.size(); j++)
    {
        btCollisionShape* shape = ant_collisionShapes[j];
        delete shape;
    }
    //delete dynamics world
    delete m_dynamicsWorld;

    //delete solver
    delete ant_solver;

    //delete broadphase
    delete ant_broadphase;

    //delete dispatcher
    delete ant_dispatcher;

    delete ant_collisionConfiguration;
}

ant** swarmDemo::getPlayers()
{
    return &ant_players[0];
}
