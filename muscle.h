#ifndef MUSCLE_H
#define MUSCLE_H
#include <fstream>
#include "btBulletDynamicsCommon.h"
using namespace std;

class muscle
{
    int index;



    btScalar activation;
    btScalar vel;
    btScalar len;

    btScalar maxForce;
    btScalar maxVel;
    btScalar staticLen;
    btScalar minLen;
    btScalar maxLen;

    btScalar staticAngle;
    //btScalar preAngle;
    btScalar levelArm;

    btScalar passiveK;
    btScalar passiveAlpha;
    btScalar fl_a0;
    btScalar fl_a1;
    btScalar fl_a2;
    btScalar fl_a3;
    btScalar fl_a4;
    btScalar fv_b;
    btScalar fv_f;

    ofstream record;
    public:
        muscle(int index, btScalar statLen, btScalar radius, btScalar maxAngle, btScalar minAngle, btScalar mF);
        virtual ~muscle();
        void setVel(btScalar vel);
        void setLen(btScalar len);
        btScalar getVel();
        btScalar getLen();
        btScalar getStaticLen();
        void activationDynamics(bool excited, btScalar excitation);
        btScalar getTorque(bool excited, btScalar excitation, btScalar angle, btScalar deltaT);
        btScalar normalizeAngle(btScalar angle);
        void updateLenVel(btScalar angle, btScalar deltaT);

        btScalar getForce();
        btScalar activeForce();
        btScalar forcevelocity();
        btScalar forcelength();
        btScalar passiveForce();

        void testFL();
        void testFV();
    protected:
    private:
};

#endif // MUSCLE_H
