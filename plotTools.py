import numpy as np
import matplotlib.pyplot as plt

class plotTools:
    numLegs = 6
    joints_per_Leg = 3
    prefixArray = ["COM Translation-", "COM Rotation-", "Joint Angles-", "Foot Contact-"]
    xyzArray = ["X", "Y", "Z"]
    legLegendArray = ["R1", "R2", "R3", "L1", "L2", "L3"];
    jointLegendArray = ["TC", "CF", "FT"];
    
    def kick(self, dataChoice, selectedColumns, isSubFig):
        data = self.readData(dataChoice, selectedColumns)
        self.plot(data, selectedColumns, isSubFig)
    
    def readData(self, dataChoice):
        fileNameList = ["position", "rotation", "angles", "contact"]
        dataInput = np.loadtxt("record/" + fileNameList[dataChoice] + ".txt")
        return dataInput
        # selDataInput = dataInput[:, selectedColumns]        
        # return selDataInput
        
    def plot(self, dataInput, legends, isSubFig):
        numRows, numCols = dataInput.shape
        if (isSubFig == True):
            plt.figure()
        for colInd in xrange(numCols):            
            if (isSubFig == True):
                axis = plt.subplot(numCols, 1, colInd+1)
            else:
                axis = plt.figure(colInd)
            # lg = self.getLegend(selectedColumns[colInd])
            plt.plot(dataInput[:, colInd], label=legends[colInd])
            minY = min(dataInput[:, colInd])
            maxY = max(dataInput[:, colInd])
            marginY = (maxY - minY)/10
            axis.set_ylim([minY - marginY, maxY + marginY])
            plt.legend()            
        plt.show()

    
    def getLegend(self, dataType, selectCols):
        legends = []
        prefix = self.prefixArray[dataType]
        if (dataType == 0) or (dataType == 1):
            for colInd in selectCols:
                legends.append(prefix+self.xyzArray[colInd])
        if (dataType == 2):            
            for colInd in selectCols:
                legLegend = self.legLegendArray[colInd/self.joints_per_Leg]
                jointLegend = self.jointLegendArray[colInd%self.joints_per_Leg]                
                legends.append(prefix+legLegend+jointLegend)
        if (dataType == 3):
            for colInd in selectCols:
                legends.append(prefix+self.legLegendArray[colInd])
        return legends
    
    def savePlot(self):
        pass
        

#p = plotTools()
#plegInd = 1
#pjointInd = [plegInd*3, plegInd*3  + 1, plegInd*3 + 2 ]
#p.kick(0, [1], True)