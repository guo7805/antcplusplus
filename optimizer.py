import collections
import numpy as np
import utils
import cma
import multiprocessing as mp
import utils
import shutil
import os
from math import pi
from scipy.optimize import fmin_tnc, fmin_powell, fmin_bfgs, fmin
import normalization as nl


def initX(useLastBest):
    '''
    paramList - Type: List. Max Angle, Min Angle, Freq, Phase, Amplitude, Offset. 9x6
    '''
    if useLastBest == True:
        paramList = []
        with open('param/geometry.cfg', 'r') as fileHandle:
            allLines = fileHandle.readlines()
            max_Angle_str = allLines[5]
            min_Angle_str = allLines[7]
            max_Angle_list = [float(e) for e in max_Angle_str.split()]
            min_Angle_list = [float(e) for e in min_Angle_str.split()]
            paramList = max_Angle_list[0:9] + min_Angle_list[0:9]
        with open('param/CPG_Controller.cfg', 'r') as fileHandle:
            allLines = fileHandle.readlines()
            selLines = [allLines[1], allLines[3], allLines[5], allLines[7]]
            for line in selLines:
                line_list = [float(e) for e in line.split()]
                paramList = paramList + line_list[0:9]
        return paramList
    else:    
        max_Angle = [-0.7854,-0.3927,1.1781,0.5236,-1.1781,1.5708,1.1781,-1.1781,1.5708]
        # 1.0472,	2.7489,	0.0000,	1.0472,	2.7489,	0.0000,	0.0000,	2.7489,	0.0000]
        min_Angle = [-1.1781,-1.1781,0.3927,-0.5236,-1.9635,0.7854,0.3927,-1.9635,0.3927]	
        # 0.0000,	1.9635,	-2.3562,	-1.0472,	1.9635,	-2.3562,	-1.0472,	1.9635,	-2.3562]
        return max_Angle, min_Angle

def initBounds(paramList, isCMA):    
    '''
    paramList - Type: list. [max_Angle, min_Angle, freq, phase, amplitude, offset]. 9x6
    '''
    max_Angle = paramList[0:9]
    min_Angle = paramList[9:18]
    freq = paramList[18:27]
    phase = paramList[27:36]
    amp = paramList[36:45]
    offset = paramList[45:54]    
    
    dRange = [ma - mi for ma, mi in zip(max_Angle, min_Angle)]
    max_Angle_ub = [m+dR*0.2 for m, dR in zip(max_Angle, dRange)]
    max_Angle_lb = [m-dR*0.2 for m, dR in zip(max_Angle, dRange)]
    min_Angle_ub = [m+dR*0.2 for m, dR in zip(min_Angle, dRange)]
    min_Angle_lb = [m-dR*0.2 for m, dR in zip(min_Angle, dRange)]

    freq_ub = [5, 5, 5, 5, 5, 5, 5, 5, 5]
    freq_lb = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    phase_ub = [6.28, 6.28, 6.28, 6.28, 6.28, 6.28, 6.28, 6.28, 6.28]
    phase_lb = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    amp_ub = [1, 1, 1, 1, 1, 1, 1, 1, 1]
    amp_lb = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    offset_ub = [1, 1, 1, 1, 1, 1, 1, 1, 1, ]
    offset_lb = [-1, -1, -1, -1, -1, -1, -1, -1, -1]

    bounds = [max_Angle_lb+min_Angle_lb+freq_lb+phase_lb+amp_lb+offset_lb, max_Angle_ub+min_Angle_ub+freq_ub+phase_ub+amp_ub+offset_ub]
    # bounds = convertBounds(max_Angle_lb, min_Angle_lb, max_Angle_ub, min_Angle_ub, isCMA)  
    return bounds

def optimizeByLimits(weightList=None, nadirUtopia=None):
    indGen = 0
    indX = 1000
    folderName = "optimizer/" + "G" + str(indGen) + "/X" + str(indX)
    if not os.path.exists(folderName):
        os.makedirs(folderName)       
    paramList = initX(True)
    boundsList = initBounds(paramList, True)
    newBounds = []
    for lb, ub in zip(boundsList[0], boundsList[1]):
        newBounds.append((lb, ub))
    with open("BFGS.txt", "w"):
        pass    

    
    xopt, fopt, direc, iterO, funcalls, warnFlags, allvecs = fmin_powell(realFitnessFun_limits, np.array(paramList), args=(weightList, nadirUtopia), maxiter=1000)
    # xopt, fopt, iters, funcalls, warnFlags, allvecs = fmin(realFitnessFun_limits, np.array(paramList), args=(weightList, nadirUtopia), maxiter=1000)
    # x, n, rc = fmin_tnc(realFitnessFun_limits, np.array(paramList), fprime=None, approx_grad=True, args=(weightList, nadirUtopia), bounds=newBounds, epsilon=1e-06)
    # not working properly
    # res = minimize(realFitnessFun_limits, np.array(paramList), jac=False, method='L-BFGS-B', bounds=newBounds)
    # xopt, fopt, direc, iterO, funcalls, warnFlags, allvecs = fmin_bfgs(realFitnessFun_limits, np.array(paramList))
    # x, n, rc = fmin_tnc(realFitnessFun_limits, np.array(paramList), approx_grad=True, bounds=None )
    
    
def realFitnessFun_limits(x, *args):
        
    indGen = 0
    indX = 1000
    saveParam(indGen, indX, x[0:18], paramType='limit')
    saveParam(indGen, indX, x[18:], paramType='cpg')
    
    simulationBody(indGen, indX)
    
    fitnessValue = computeObjValue(indGen, indX, weightArray=args[0], nadirUtopia=args[1])
    with open("BFGS.txt", "a") as fileHandle:
        outStr = str(fitnessValue) + " " + list2string(x) + "\n"
        fileHandle.write(outStr)
    return fitnessValue    

    

def optimizeByCMA(useMP=False, objSel=None, weightArray=None, nadirUtopia=None):
    paramList = initX(True)        
    bounds = initBounds(paramList, True)
    
    # es = cma.CMAEvolutionStrategy(xinit, 0.5, {'bounds': bounds, 'scaling_of_variables': scaleV})
    es = cma.CMAEvolutionStrategy(paramList, 0.2, {'bounds': bounds})
    logger = cma.CMADataLogger()  # with default name   
    indGen = 0
    
    if os.path.exists("optimizer/"):
        shutil.rmtree("optimizer/")
    os.mkdir("optimizer")
    
    if useMP:
        while not es.stop():
            
            os.mkdir("optimizer/" + "G" + str(indGen))
            pool = mp.Pool(15)            
            results = []
            workerList = []
            X = es.ask()
            for indX, x in enumerate(X):
                workerList.append(pool.apply_async(realFitnessFun, args = (x, indGen, indX), kwds={'objSel':objSel, 'weightArray':weightArray, 'nadirUtopia':nadirUtopia}))
            pool.close()
            pool.join()
            for worker in workerList:
                if worker.successful():
                    results.append(worker.get())
                else:
                    results.append(np.NaN)   
            logger.add(es=es)
            es.tell(X, results)
            es.disp(20)    
            indGen += 1
    else:
        while not es.stop(): # iterate   
            os.mkdir("optimizer/" + "G" + str(indGen))
            X = es.ask()     # get candidate solutions
            f = [realFitnessFun(x, indGen, indX, objSel=objSel, weightArray=weightArray, nadirUtopia=nadirUtopia) for indX, x in enumerate(X)]  # evaluate solutions
            #  maybe do something else that needs to be done 
            es.tell(X, f)    # do all the real work: prepare for next iteration
            es.disp(20)      # display info every 20th iteration        
            logger.add(es=es)
            indGen += 1
    return es.best.f
    # utils.cleanup()


def convertBounds(max_Angle_lb, min_Angle_lb, max_Angle_ub, min_Angle_ub, isCMA):
    bounds = []
    if isCMA:
        bounds = [max_Angle_lb+min_Angle_lb, max_Angle_ub+min_Angle_ub]
    else:        
        for lb_e, ub_e in zip(max_Angle_lb, max_Angle_ub):
            bounds.append((lb_e, ub_e))
        for lb_e, ub_e in zip(min_Angle_lb, min_Angle_ub):
            bounds.append((lb_e, ub_e))
    return bounds

def saveParam(indGen, indX, x, paramType):
    
    dim = len(x)
    cat = dim//9
    if paramType == 'limit':
        max_Angle = np.array(x[0:9])
        min_Angle = np.array(x[9:18])
        max_Angle_list = np.append(max_Angle, -min_Angle)
        min_Angle_list = np.append(min_Angle, -max_Angle)
        max_Angle_str = list2string(max_Angle_list)
        min_Angle_str = list2string(min_Angle_list)
        limitInfo = "#Max\n" + max_Angle_str + "\n#Min\n" + min_Angle_str    
        with open("param/skeleton.cfg", "r") as inFile:
            skeleton = inFile.read()    
        with open("optimizer/G" + str(indGen) + "/X" + str(indX) + "/geometry.cfg", "w") as outFile:
            outFile.write(skeleton+limitInfo)
    else:
        fileStr = ''
        for indCat in range(cat):
            paramList = x[indCat*9:indCat*9 + 9]
            paramList2 = np.append(paramList, paramList)
            paramStr = list2string(paramList2)
            fileStr = fileStr + 'heading line\n' + paramStr + '\n'
        with open("optimizer/G" + str(indGen) + "/X" + str(indX) + "/CPG_Controller.cfg", "w") as outFile:
            outFile.write(fileStr)
    


def list2string(inList):
    outString = ' '.join( "%.6f" % e for e in inList)
    return outString
    
def string2list():
    pass

def simulationBody(indGen, indX):
    # strMax = ' '.join([str(e) for e in max_Angle])
    # strMin = ' '.join([str(e) for e in min_Angle])
    cmd = 'bin/Optimization/ant' + ' ' + str(indGen) + ' ' + str(indX)
    os.system(cmd)

def realFitnessFun(x, indGen, indX, objSel=None, weightArray=None, nadirUtopia=None):        
    folderName = "optimizer/" + "G" + str(indGen) + "/X" + str(indX)    
    if not os.path.exists(folderName):
        os.makedirs(folderName)       
    
    saveParam(indGen, indX, x[0:18], paramType='limit')
    saveParam(indGen, indX, x[18:], paramType='cpg')
    
    simulationBody(indGen, indX)
    
    fitnessValue = computeObjValue(indGen, indX, objSel=objSel, weightArray=weightArray, nadirUtopia=nadirUtopia)
    return fitnessValue


def getNadirUtopiaPoints():
    paramList = initX(True)
    boundsList = initBounds(paramList, True)
    

    


def computeObjValue(indGen, indX, scalar=True, objSel=None, weightArray=None, nadirUtopia=None):    
    
    posArray = np.loadtxt("optimizer/G" + str(indGen) + "/X" + str(indX) + "/position.txt")
    rotArray = np.loadtxt("optimizer/G" + str(indGen) + "/X" + str(indX) + "/rotation.txt")
    
    if nadirUtopia == None:
        devPen = deviationPenalty(posArray)
        rotPen = rotationPenalty(rotArray)    
        velPen = velocityPenalty(posArray)
    else:
        devPen = deviationPenalty(posArray, normalize=True, nadir=nadirUtopia[0][0], utopia=nadirUtopia[1][0])
        rotPen = rotationPenalty(rotArray, normalize=True, nadir=nadirUtopia[0][1], utopia=nadirUtopia[1][1])    
        velPen = velocityPenalty(posArray, normalize=True, nadir=nadirUtopia[0][2], utopia=nadirUtopia[1][2])        
        
    
    if objSel is not None:
        if objSel == 'dev':
            return devPen
        elif objSel == 'rot':
            return rotPen
        elif objSel == 'vel':
            return velPen
    
    # calculate the fitness
    if scalar == True:
        if weightArray is not None:
            w = [weightArray[0], weightArray[1], 1-weightArray[0]-weightArray[1]]
        else:
            w = [1/3, 1/3, 1/3]       
        fitness = sum(map(lambda x, y: x*y, [devPen, rotPen, velPen], w))        
        print(fitness)
    else:
        fitness = [devPen, rotPen, velPen]
    return fitness

def torquePenalty():
    pass
    
def deviationPenalty(posArray, normalize=False, utopia=0.0, nadir=1.0):
    # constraining the lateral and vertical deviations of COM
    # xError = 0.0    
    yError = 0.0
    zError = 0.0
    yPosArray = posArray[:,1]
    zPosArray = posArray[:,2]
    
    targetYPos = 0.45
    targetZPos = 0.0
    
    yError = (yPosArray - targetYPos).T * (yPosArray - targetYPos)
    zError = (zPosArray - targetZPos).T * (zPosArray - targetZPos)
    errorSum = sum(yError) + sum(zError)
    if normalize == True:
        errorSum = (errorSum - utopia)/(nadir - utopia)
    return errorSum

def rotationPenalty(rotArray, normalize=False, utopia=0.0, nadir=1.0):
    xError = 0.0
    yError = 0.0
    zError = 0.0    
    xRotArray = rotArray[:,0]
    yRotArray = rotArray[:,1]
    zRotArray = rotArray[:,2]
    
    targetXRot = 0.0
    targetYRot = 0.0
    targetZRot = 0.0
    
    xError = (xRotArray - targetXRot).T * (xRotArray - targetXRot)
    # yError = (yRotArray - targetYRot).T * (yRotArray - targetYRot)\
    yError = [0]
    zError = (zRotArray - targetZRot).T * (zRotArray - targetZRot)
    errorSum = sum(xError) + sum(zError)
    if normalize == True:
        errorSum = (errorSum - utopia)/(nadir - utopia)    
    return errorSum

def velocityPenalty(posArray, normalize=False, utopia=0.0, nadir=1.0):
    # constraining the differences between the constant target velocity and the actual moving velocity
    velError = 0.0    
    xPosArray = posArray[:,0]
    # resample again to 10Hz, each datapoint is collected every 100 ms
    xPosArray = xPosArray[0:-1:10]
    
    targetVelocity = 1.0    
    diffXPosArray = np.diff(xPosArray)
    
    velError = (diffXPosArray - targetVelocity*0.1).T * (diffXPosArray - targetVelocity*0.1)
    
    totalTime = len(xPosArray)/10
    
    maxX = max(xPosArray)
    
    sumVelError = sum(velError)
    maxXError = (totalTime - maxX)*(totalTime - maxX)
    errorSum = sumVelError + maxXError
    if normalize == True:
        errorSum = (errorSum - utopia)/(nadir - utopia)    
    return errorSum

def jointPenalty():
    rangePenalty = 0.0
    if (angle > maxA):
        rangePenalty = rangePenalty + (angle - maxA)*(angle - maxA)
    if (angle < minA):
        rangePenalty = rangePenalty + (angle - minA)*(angle - minA)    
    return rangePenalty

def speedPenalty():
    speedPenalty = 0.0
    if traj_onedof and (angle - traj_onedof[-1] > 0.01):
        speedPenalty = speedPenalty + (angle - traj_onedof[-1])*(angle - traj_onedof[-1])
    return speedPenalty

def trajPenalty():
    pass

    