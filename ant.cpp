#include "ant.h"
#include "utils.h"
#include <iostream>
#include <sstream>
#include <cstring>
#include <fstream>
using namespace std;
// LOCAL FUNCTIONS







void ant::readCpgParam(btScalar m_cpgFreq[], btScalar m_cpgPhase[], btScalar m_cpgAmp[], btScalar m_cpgOffset[])
{
    ifstream gfile;
    #ifdef OPTIMIZATION
        std::stringstream ss;
        ss << "optimizer/G" << m_indGen << "/X" << m_indX << "/CPG_Controller.cfg";
        std::string fileName = ss.str();
        gfile.open(fileName.c_str());
    #elif defined FLUT
        gfile.open("flut/CPG_Controller.cfg");
    #else
        gfile.open("param/CPG_Controller.cfg");
    #endif

    string oneline;
    if (gfile.is_open())
    {
        // read in the body length
        getline(gfile, oneline);
        getline(gfile, oneline);
        istringstream iss_l(oneline);
        for(int i = 0; i < NUM_JOINT; i++)
        {
            iss_l >> m_cpgFreq[i];
        }

        // raed in the body radius
        getline(gfile, oneline);
        getline(gfile, oneline);
        istringstream iss_r(oneline);
        for(int i = 0; i < NUM_JOINT; i++)
        {
            iss_r >> m_cpgPhase[i];
        }

        // raed in the max joint limits
        getline(gfile, oneline);
        getline(gfile, oneline);
        istringstream iss_max(oneline);
        for(int i = 0; i < NUM_JOINT; i++)
        {
            iss_max >> m_cpgAmp[i];
        }

        // raed in the min joint limits
        getline(gfile, oneline);
        getline(gfile, oneline);
        istringstream iss_min(oneline);
        for(int i = 0; i < NUM_JOINT; i++)
        {
            iss_min >> m_cpgOffset[i];
        }
    }
    else
    {
        cout << "Error Opening the Configuration File for the CPG controller" << endl;
    }
    gfile.close();

}

void ant::readParam(btScalar m_bodylength[NUM_BODYPARTS], btScalar m_bodyradius[NUM_BODYPARTS], btScalar m_maxAngle[NUM_JOINT], btScalar m_minAngle[NUM_JOINT])
{
    ifstream gfile;
    #ifdef OPTIMIZATION
        std::stringstream ss;
        ss << "optimizer/G" << m_indGen << "/X" << m_indX << "/geometry.cfg";
        std::string fileName = ss.str();
        gfile.open(fileName.c_str());
    #elif defined FLUT
        gfile.open("flut/geometry.cfg");
    #else
        gfile.open("param/geometry.cfg");
    #endif

    string oneline;
    if (gfile.is_open())
    {
        // read in the body length
        getline(gfile, oneline);
        getline(gfile, oneline);
        istringstream iss_l(oneline);
        for(int i = 0; i < NUM_BODYPARTS; i++)
        {
            iss_l >> m_bodylength[i];
        }

        // raed in the body radius
        getline(gfile, oneline);
        getline(gfile, oneline);
        istringstream iss_r(oneline);
        for(int i = 0; i < NUM_BODYPARTS; i++)
        {
            iss_r >> m_bodyradius[i];
        }

        // raed in the max joint limits
        getline(gfile, oneline);
        getline(gfile, oneline);
        istringstream iss_max(oneline);
        for(int i = 0; i < NUM_JOINT; i++)
        {
            iss_max >> m_maxAngle[i];
        }

        // raed in the min joint limits
        getline(gfile, oneline);
        getline(gfile, oneline);
        istringstream iss_min(oneline);
        for(int i = 0; i < NUM_JOINT; i++)
        {
            iss_min >> m_minAngle[i];
        }
    }
    else
    {
        cout << "Error Opening the Configuration File for the Geometry" << endl;
    }
    gfile.close();
}

void ant::writeParam(btScalar maxAngle[NUM_BODYPARTS], btScalar minAngle[NUM_BODYPARTS])
{
    ofstream ofile;
    ofile.open("param.cfg");
    ofile.precision(4);
    if (ofile.is_open())
    {
        ofile << "# Max Joint Limits" << endl;;
        for (int i = 0; i < NUM_JOINT; i++)
        {
            ofile << std::fixed << maxAngle[i] << "\t";
        }
        ofile << endl;

        ofile << "# Min Joint Limits" << endl;;
        for (int i = 0; i < NUM_JOINT; i++)
        {
            ofile << std::fixed << minAngle[i] << "\t";
        }
        ofile << endl;
    }
    else
    {
        cout << "Error Writing the parameters into the File" << endl;
    }
    ofile.close();
}

void ant::keepRecord(bool position, bool rotation, bool angles, bool contact)
{
    int sampleRate = 10;
    if (position==true)
    {
        ofstream outFile;
        outFile.precision(4);
        #ifdef OPTIMIZATION
            std::stringstream ss;
            ss << "optimizer/G" << m_indGen << "/X" << m_indX << "/position.txt";
            std::string fileName = ss.str();
            outFile.open(fileName.c_str(), std::ofstream::out | std::ofstream::trunc);
        #elif defined FLUT
            outFile.open("flut/position.txt", std::ofstream::out | std::ofstream::trunc);
        #else
            outFile.open("record/position.txt", std::ofstream::out | std::ofstream::trunc);
        #endif

        if (outFile.is_open())
        {
            for (int i = 0; i < positionHistory.size(); i++)
            {
                if (i%sampleRate == 0)
                    outFile << std::fixed << positionHistory[i].getX() << " " << positionHistory[i].getY() << " " << positionHistory[i].getZ() << endl;
            }
            outFile.close();
        }
        else
        {
            cout << "Error Writing the history of COM position" << endl;
        }
    }
    if (rotation==true)
    {
        ofstream outFile;
        outFile.precision(4);
        #ifdef OPTIMIZATION
            std::stringstream ss;
            ss << "optimizer/G" << m_indGen << "/X" << m_indX << "/rotation.txt";
            std::string fileName = ss.str();
            outFile.open(fileName.c_str(), std::ofstream::out | std::ofstream::trunc);
        #elif defined FLUT
            outFile.open("flut/rotation.txt", std::ofstream::out | std::ofstream::trunc);
        #else
            outFile.open("record/rotation.txt", std::ofstream::out | std::ofstream::trunc);
        #endif
        if (outFile.is_open())
        {
            for (int i = 0; i < rotationHistory.size(); i++)
            {
                if (i%sampleRate == 0)
                    outFile << std::fixed << rotationHistory[i].getX() << " " << rotationHistory[i].getY() << " " << rotationHistory[i].getZ() << endl;
            }
            outFile.close();
        }
        else
        {
            cout << "Error Writing the history of COM rotation" << endl;
        }
        outFile.close();
    }
    if (angles == true)
    {
        ofstream outFile;
        outFile.precision(4);
        #ifdef OPTIMIZATION
            std::stringstream ss;
            ss << "optimizer/G" << m_indGen << "/X" << m_indX << "/angles.txt";
            std::string fileName = ss.str();
            outFile.open(fileName.c_str(), std::ofstream::out | std::ofstream::trunc);
        #elif defined FLUT
            outFile.open("flut/angles.txt", std::ofstream::out | std::ofstream::trunc);
        #else
            outFile.open("record/angles.txt", std::ofstream::out | std::ofstream::trunc);
        #endif
        if (outFile.is_open())
        {
            for (int i = 0; i < angleHistory.size(); i++)
            {
                if (i%sampleRate == 0)
                {
                    for (int j = 0; j < NUM_LEGS; j++)
                        outFile << std::fixed << angleHistory[i][j].getX() << " " << angleHistory[i][j].getY() << " " << angleHistory[i][j].getZ() << " ";
                    outFile << endl;
                }
            }
            outFile.close();
        }
        else
        {
            cout << "Error Writing the history of joint angles" << endl;
        }
        outFile.close();
    }
    if (contact == true)
    {
        ofstream outFile;
        outFile.precision(4);
        #ifdef OPTIMIZATION
            std::stringstream ss;
            ss << "optimizer/G" << m_indGen << "/X" << m_indX << "/contact.txt";
            std::string fileName = ss.str();
            outFile.open(fileName.c_str(), std::ofstream::out | std::ofstream::trunc);
        #elif defined FLUT
            outFile.open("flut/contact.txt", std::ofstream::out | std::ofstream::trunc);
        #else
            outFile.open("record/contact.txt", std::ofstream::out | std::ofstream::trunc);
        #endif
        if (outFile.is_open())
        {
            for (int i = 0; i < contactHistory.size(); i++)
            {
                if (i%sampleRate == 0)
                {
                    for (int j = 0; j < NUM_LEGS; j++)
                        outFile << std::fixed << contactHistory[i][j] << " ";  //<< contactHistory[i][j] << " " << contactHistory[i][j] << " ";
                    outFile << endl;
                }
            }
            outFile.close();
        }
        else
        {
            cout << "Error Writing the history of joint angles" << endl;
        }
        outFile.close();
    }
}

// END OF LOCAL FUNCTIONS

btRigidBody* ant::localCreateRigidBody (btScalar mass, const btTransform& startTransform, btCollisionShape* shape, short group, short mask)
{
    m_fCyclePeriod = 2000.0;
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0,0,0);
    if (isDynamic)
        shape->calculateLocalInertia(mass,localInertia);

    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,shape,localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);


    m_ownerWorld->addRigidBody(body, group, mask);

    //body->setGravity(btVector3(0.0, 0.0, 0.0));

    return body;
}

//ant::ant(btDynamicsWorld* ownerWorld) : m_ownerWorld (ownerWorld),
//m_bodylength {1.0f, 0.15f, 0.5f, 0.5f, 0.15f, 0.75f, 0.75f, 0.15f, 1.0f, 1.0f, 0.15f, 0.5f, 0.5f, 0.15f, 0.75f, 0.75f, 0.15f, 1.0f, 1.0f},
//m_bodyradius {0.35, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1},
//m_maxAngle {0.f, -M_PI/8*5, M_PI_4*3, M_PI/3, -M_PI/8*5, M_PI_4*3, M_PI/3, -M_PI/8*5, M_PI_4*3, M_PI/3, M_PI/8*7, 0.0, M_PI/3, M_PI/8*7, 0.0, 0.0f, M_PI/8*7, 0.0},
//m_minAngle {-M_PI/3, -M_PI/8*7, 0.0, -M_PI/3, -M_PI/8*7, 0.0, 0.0, -M_PI/8*7, 0.0, 0.0, M_PI/8*5, -M_PI_4*3, -M_PI/3, M_PI/8*5, -M_PI_4*3, -M_PI/3, M_PI/8*5, -M_PI_4*3}
ant::ant(btDynamicsWorld* ownerWorld) : m_ownerWorld (ownerWorld)
{

    //tarsusPointer = new tarsus();

    // m_footStatus = {true, false, true, false, true, false};
    m_footStatus = {false, false, false, false, false, false};
    readParam(m_bodylength, m_bodyradius, m_maxAngle, m_minAngle);
    readCpgParam(m_cpgFreq, m_cpgPhase, m_cpgAmp, m_cpgOffset);

    // m_percent[NUM_LEGS] = 0.0;

    setupGeometry();

}



ant::ant(btDynamicsWorld* ownerWorld, int indGen, int indX) : m_ownerWorld (ownerWorld)
{
    #ifdef OPTIMIZATION
    m_indGen = indGen;
    m_indX = indX;
    #endif
    m_footStatus = {false, false, false, false, false, false};
    readParam(m_bodylength, m_bodyradius, m_maxAngle, m_minAngle);
    readCpgParam(m_cpgFreq, m_cpgPhase, m_cpgAmp, m_cpgOffset);
    setupGeometry();
}

void ant::setupGeometry()
{

    btVector3 thoraxCOM;
    thoraxCOM = btVector3(btScalar(0.0), btScalar(1.1), btScalar(0.0));
    btTransform tr_global;
    tr_global.setIdentity();

    tr_global.setOrigin(thoraxCOM);

    //readParam(&m_bodylength, &m_bodyradius);

    // btScalar thoraxRad, thoraxLen, thoraxMass;
    btScalar coxaRad, coxaLen, coxaMass;
    btScalar femurRad, femurLen, femurMass;
    btScalar tibiaRad, tibiaLen, tibiaMass;

    btScalar thoraxRad = m_bodyradius[0];
    btScalar thoraxLen = m_bodylength[0];
    btScalar thoraxMass = thoraxRad*thoraxRad*M_PI*thoraxLen;


//Setup the Geometry
    m_shapes[0] = new btCapsuleShapeX(thoraxRad, thoraxLen);
    //thoraxMass = 0.f;
    m_bodies[0] = localCreateRigidBody(thoraxMass, tr_global, m_shapes[0], COL_BODY, bodyCollidesWith);

    btScalar head_radius = 0.1;
    btScalar head_mass = M_PI*head_radius*head_radius*head_radius/3*4;
    btCollisionShape* head_shape = new btSphereShape(head_radius);
    btTransform tr_head;
    tr_head.setIdentity();
    tr_head.setOrigin(btVector3(thoraxLen/2+head_radius, 0, 0));
    head_body = localCreateRigidBody(head_mass, tr_global*tr_head, head_shape, COL_BODY, bodyCollidesWith);
    btConeTwistConstraint* neck;
    btTransform neckInThorax, neckInHead;
    neckInThorax.setIdentity();
    neckInHead.setIdentity();
    neckInThorax.setOrigin(btVector3(thoraxLen/2, 0, 0));
    neckInHead.setOrigin(btVector3(-head_radius, 0, 0));
    neck = new btConeTwistConstraint(*m_bodies[0], *head_body, neckInThorax, neckInHead);
    neck->setLimit(0.0, 0.0, 0.0);
    m_ownerWorld->addConstraint(neck, true);

    btScalar abdomen_radius = 0.15;
    btScalar abdomen_height = 0.05;
    btScalar abdomen_mass = M_PI*abdomen_radius*abdomen_radius*abdomen_height;
    btCollisionShape* abdomen_shape = new btCapsuleShapeX(abdomen_radius, abdomen_height);
    btTransform tr_abdomen;
    tr_abdomen.setIdentity();
    tr_abdomen.setOrigin(btVector3(-thoraxLen/2-head_radius, 0, 0));
    abdomen_body = localCreateRigidBody(abdomen_mass/5.0, tr_global*tr_abdomen, abdomen_shape, COL_BODY, bodyCollidesWith);
    btConeTwistConstraint* hip;
    btTransform hipInThorax, hipInAbdomen;
    hipInThorax.setIdentity();
    hipInAbdomen.setIdentity();
    hipInThorax.setOrigin(btVector3(-thoraxLen/2, 0, 0));
    hipInAbdomen.setOrigin(btVector3(abdomen_radius, 0, 0));
    hip = new btConeTwistConstraint(*m_bodies[0], *abdomen_body, hipInThorax, hipInAbdomen);
    hip->setLimit(0.0, 0.0, 0.0);
    m_ownerWorld->addConstraint(hip, true);


    // transform matrix for COM of coxa, femur and tibia
    btTransform tr_coxaCOM, tr_femurCOM, tr_tibiaCOM;

    // transform matrix for constraint frames of thorax-coxa, coxa-femur, femur-tibia
    btTransform tr_tcFrame, tr_cfFrame, tr_ftFrame;

    for (int i = 0; i<NUM_LEGS; i++)
    {

        coxaRad = m_bodyradius[1+i*3];
        coxaLen = m_bodylength[1+i*3];
        coxaMass = coxaLen*coxaRad*M_PI*coxaRad;
        //m_shapes[1+i*3] = new btCapsuleShape(coxaRad, coxaLen);

        femurRad = m_bodyradius[2+i*3];
        femurLen = m_bodylength[2+i*3];
        femurMass = femurLen*femurRad*M_PI*femurRad;
        //m_shapes[2+i*3] = new btCapsuleShape(femurRad, femurLen);

        tibiaRad = m_bodyradius[3+i*3];
        tibiaLen = m_bodylength[3+i*3];
        tibiaMass = tibiaLen*tibiaRad*M_PI*tibiaRad;
        //m_shapes[3+i*3] = new btCapsuleShape(tibiaRad, tibiaLen);

        btTransform tr_TCinCOM, tr_COXAinTC, tr_CFinCOXA, tr_FEMURinCF, tr_FTinFEMUR, tr_TIBIAinFT;
        tr_TCinCOM.setIdentity();
        btScalar thoraxX, thoraxZ;
        switch (i)
        {
        case 0:
            thoraxX = thoraxLen/2;
            thoraxZ = thoraxRad;
            break;
        case 1:
            thoraxX = 0;
            thoraxZ = thoraxRad;
            break;
        case 2:
            thoraxX = -thoraxLen/2;
            thoraxZ = thoraxRad;
            break;
        case 3:
            thoraxX = thoraxLen/2;
            thoraxZ = -thoraxRad;
            break;
        case 4:
            thoraxX = 0;
            thoraxZ = -thoraxRad;
            break;
        case 5:
            thoraxX = -thoraxLen/2;
            thoraxZ = -thoraxRad;
            break;
        }

        tr_TCinCOM.setOrigin(btVector3(thoraxX, btScalar(0.0), thoraxZ));
        btQuaternion quat_TC2COM(btVector3( btScalar(1.0), btScalar(0.0), btScalar(0.0)), -M_PI_2);
        tr_TCinCOM.setRotation(quat_TC2COM);


        tr_COXAinTC.setIdentity();
        if (i < NUM_LEGS/2)
        {
            tr_COXAinTC.setOrigin(btVector3(btScalar(0.0), -coxaLen/2,  btScalar(0.0)));
        }
        else
        {
            tr_COXAinTC.setOrigin(btVector3(btScalar(0.0), coxaLen/2,  btScalar(0.0)));
        }
        btQuaternion quat_COXAinTC(btVector3(btScalar(1.0), btScalar(0.0), btScalar(0.0)), M_PI_2);
        tr_COXAinTC.setRotation(quat_COXAinTC);

        tr_CFinCOXA.setIdentity();
        if (i < NUM_LEGS/2)
        {
            tr_CFinCOXA.setOrigin(btVector3(btScalar(0.0), btScalar(0.0), coxaLen/2));
        }
        else
        {
            tr_CFinCOXA.setOrigin(btVector3(btScalar(0.0), btScalar(0.0), -coxaLen/2));
        }
        btQuaternion quat_CFinCOXA(btVector3(btScalar(0.0), btScalar(1.0),  btScalar(0.0)), -M_PI_2);
        tr_CFinCOXA.setRotation(quat_CFinCOXA);

        tr_FEMURinCF.setIdentity();
        tr_FEMURinCF.setOrigin(btVector3(btScalar(0.0), -femurLen/2,  btScalar(0.0)));
        btQuaternion quat_FEMURinCF(btVector3( btScalar(0.0), btScalar(1.0), btScalar(0.0)), M_PI_2);
        tr_FEMURinCF.setRotation(quat_FEMURinCF);

        tr_FTinFEMUR.setIdentity();
        tr_FTinFEMUR.setOrigin(btVector3(btScalar(0.0), -femurLen/2, btScalar(0.0)));
        btQuaternion quat_FTinFEMUR(btVector3( btScalar(0.0), btScalar(1.0), btScalar(0.0)), -M_PI_2);
        tr_FTinFEMUR.setRotation(quat_FTinFEMUR);

        tr_TIBIAinFT.setIdentity();
        tr_TIBIAinFT.setOrigin(btVector3(btScalar(0.0), -tibiaLen/2, btScalar(0.0)));
        btQuaternion quat_TIBIAinFT(btVector3( btScalar(0.0), btScalar(1.0), btScalar(0.0)), M_PI_2);
        tr_TIBIAinFT.setRotation(quat_TIBIAinFT);

        btTransform tr_coxa = tr_global*tr_TCinCOM*tr_COXAinTC;
        m_shapes[i*3+1] = new btCapsuleShapeZ(coxaRad, coxaLen);
        m_bodies[i*3+1] = localCreateRigidBody(coxaMass, tr_coxa, m_shapes[i*3+1], COL_BODY, bodyCollidesWith);


        btTransform tr_femur = tr_coxa*tr_CFinCOXA*tr_FEMURinCF;
        m_shapes[i*3+2] = new btCapsuleShape(femurRad, femurLen);
        m_bodies[i*3+2] = localCreateRigidBody(femurMass, tr_femur, m_shapes[i*3+2], COL_BODY, bodyCollidesWith);


        btTransform tr_tibia = tr_femur*tr_FTinFEMUR*tr_TIBIAinFT;
        m_shapes[i*3+3] = new btCapsuleShape(tibiaRad, tibiaLen);
        m_bodies[i*3+3] = localCreateRigidBody(tibiaMass, tr_tibia, m_shapes[i*3+3], COL_BODY, bodyCollidesWith);
        m_bodies[i*3+3]->setFriction(1000);
        //m_bodies[i*3+3]->setCollisionFlags(4);
        // m_bodies[i*3+3]->setContactProcessingThreshold(0.05);


        //End of Settingup Geometry

        btHingeConstraint* thoraxCoxa;
        btHingeConstraint* coxaFemur;
        btHingeConstraint* femurTibia;

        thoraxCoxa = new btHingeConstraint(*m_bodies[0], *m_bodies[i*3+1], tr_TCinCOM, tr_COXAinTC.inverse());
        thoraxCoxa->setLimit(m_minAngle[i*3], m_maxAngle[i*3],  1);
        m_joints[i*3] = thoraxCoxa;
        m_ownerWorld->addConstraint(m_joints[i*3], true);

        coxaFemur = new btHingeConstraint(*m_bodies[i*3+1], *m_bodies[i*3+2], tr_CFinCOXA, tr_FEMURinCF.inverse());
        coxaFemur->setLimit(m_minAngle[i*3+1], m_maxAngle[i*3+1],  1);
        m_joints[i*3+1] = coxaFemur;
        m_ownerWorld->addConstraint(m_joints[i*3+1], true);

        femurTibia = new btHingeConstraint(*m_bodies[i*3+2], *m_bodies[i*3+3], tr_FTinFEMUR, tr_TIBIAinFT.inverse());
        femurTibia->setLimit(m_minAngle[i*3+2], m_maxAngle[i*3+2],  1);
        m_joints[i*3+2] = femurTibia;
        m_ownerWorld->addConstraint(m_joints[i*3+2], true);
    }


    for (int j = 0; j < NUM_BODYPARTS; j++)
    {
        m_bodies[j]->setDamping(0.05, 0.85);
        m_bodies[j]->setDeactivationTime(0.8);
        m_bodies[j]->setSleepingThresholds(0.5f, 0.5f);
    }


}

void ant::setupNeuronMuscle()
{
     // set the muscles and neurons

    ifstream mfile, nfile;
    mfile.open("param/muscle.cfg");
    nfile.open("param/neuron.cfg");


    float muscleParam[NUM_JOINT*2], neuronParam[NUM_JOINT*2];
    string val;

    if (mfile.is_open())
    {
        getline(mfile, val);
        getline(mfile, val);
        istringstream iss(val);
        for (int i = 0; i < 6; i++)
        {
            iss >> muscleParam[i] ;
        }
    }
    else
    {
        cout << "Error Opening the Configuration File for the Muscles" << endl;
    }

    if (nfile.is_open())
    {
        getline(nfile, val);
        getline(nfile, val);
        istringstream iss(val);
        for (int i = 0; i < 6; i++)
        {
            iss >> neuronParam[i] ;
        }
    }
    else
    {
        cout << "Error Opening the Configuration File for the Muscles" << endl;
    }

    for (int i = 0; i < NUM_JOINT; i++)
    {
        // Even index is the extensor
        // Odd index is the flexor

        // m_muscles[2*i] = buildMuscles(2*i, m_bodylength[1+i], m_bodyradius[i+1], m_maxAngle[i], m_minAngle[i]);
        // m_muscles[2*i+1] = buildMuscles(2*i+1, m_bodylength[1+i], m_bodyradius[i+1], m_maxAngle[i], m_minAngle[i]);
        // m_neurons[2*i] = buildNeurons(2*i);
        // m_neurons[2*i+1] = buildNeurons(2*i+1);
        m_muscles[2*i] = new muscle(2*i, m_bodylength[1+i], m_bodyradius[i+1], m_maxAngle[i], m_minAngle[i], muscleParam[2*i]);
        m_muscles[2*i+1] = new muscle(2*i+1, m_bodylength[1+i], m_bodyradius[i+1], m_maxAngle[i], m_minAngle[i], muscleParam[2*i+1]);
        m_neurons[2*i] = new neuron(2*i, neuronParam[2*i]);
        m_neurons[2*i+1] = new neuron(2*i+1, neuronParam[2*i+1]);
    }

    mfile.close();
    nfile.close();
}



btTransform ant::getConstraintFrame(int indexJoint)
{

    btHingeConstraint* HingeC = static_cast<btHingeConstraint*>(this->getJoints()[indexJoint]);
    btTransform localB = HingeC->getBFrame();
    btTransform worldB = HingeC->getRigidBodyB().getWorldTransform();
    return worldB*localB;

}

void ant::applyTorqueOnJoint(btHingeConstraint* joint, btScalar torqueS)
{
    btVector3 torqueV = btVector3(0.0, 0.0, torqueS);
    btHingeConstraint* hingeC = static_cast<btHingeConstraint*>(joint);
    //btVector3 hingeAxisLocal = hingeC->getAFrame().getBasis().getColumn(2); // z-axis of constraint frame
    //btVector3 hingeAxisWorld = hingeC->getRigidBodyA().getWorldTransform().getBasis() * hingeAxisLocal;
    btVector3 torqueLocal = hingeC->getAFrame().getBasis()*torqueV;
    btVector3 hingeTorque = hingeC->getRigidBodyA().getWorldTransform().getBasis() * torqueLocal;
    hingeC->getRigidBodyA().applyTorque(hingeTorque);
    hingeC->getRigidBodyB().applyTorque(-hingeTorque);

}

void ant::getDOFsOneLeg(int legIndex, btScalar DOFs[3])
{

//    DOFs = new btScalar[3];
    btHingeConstraint* joint;
    for (int i = 0; i < 3; i++)
    {
        joint = this->getJoints()[legIndex*3 + i];
        DOFs[i] = joint->getHingeAngle();
    }
    // return DOFs;
}

btScalar fakeAngle(btScalar percent)
{
    if (percent < 0.5)
    {
        return 2*M_PI/3*percent;
    }
    else
    {
        return 2*M_PI/3*(1 - percent);
    }

}


void ant::driveJoint(btScalar fTargetPercent, btScalar timeStep)
{
    for (int i = 0; i < 3; i++)
    {

        btHingeConstraint* joint = this->getJoints()[i];
        btScalar angle = joint->getHingeAngle();

        neuron* n_ext = static_cast<neuron*>(this->getNeurons()[i*2]);
        neuron* n_flx = static_cast<neuron*>(this->getNeurons()[i*2+1]);

        muscle* m_ext = static_cast<muscle*>(this->getMuscles()[i*2]);
        muscle* m_flx = static_cast<muscle*>(this->getMuscles()[i*2+1]);


        btScalar n_flx_v, n_ext_v;
        btScalar m_flx_t, m_ext_t;


        if (fTargetPercent > btScalar(0.5))
        {
            n_flx_v = n_flx->activeS(timeStep, fTargetPercent);
            n_ext_v = n_ext->restS();
            //n_ext->activeS(timestep);
            // calculate the torques from both flexor and extensor muscles
            m_flx_t = m_flx->getTorque(true, n_flx_v, angle, timeStep);
            m_ext_t = m_ext->getTorque(false, n_ext_v, angle, timeStep);
        }
        else
        {
            n_flx_v = n_flx->restS();
            //n_flx->activeS(timestep);
            n_ext_v = n_ext->activeS(timeStep, fTargetPercent);
            m_flx_t = m_flx->getTorque(false, n_flx_v, angle, timeStep);
            m_ext_t = m_ext->getTorque(true, n_ext_v, angle, timeStep);
        }

        //btScalar m_flx_t = m_flx->getTorque(n_flx_v);
        //btScalar m_ext_t = m_ext->getTorque(n_ext_v);

        btScalar torque = m_flx_t - m_ext_t;
        // apply torque on each rigidbody part
        //this->applyTorqueOnJoint(joint, torque/1000);

    }
    //drawDebugFrame();
    btTransform Eye;
    Eye.setIdentity();
    drawFrame(Eye);

}

vector<btVector3> ant::readAllJointAngles()
{
    vector<btVector3> angles;
    btVector3 angle_one_leg;
    for (int legInd = 0; legInd < NUM_LEGS; legInd++ )
    {
        for (int jointInd = 0; jointInd < 3; jointInd++ )
        {
            angle_one_leg[jointInd] = m_joints[legInd*3 + jointInd]->getHingeAngle();
        }
        angles.push_back(angle_one_leg);
    }
    return angles;
}

void ant::driveJointByMotor(btScalar m_Time, btScalar timeStep)
{

    btScalar m_fMotorStrength = 0.01f;

    for (int legInd = 0; legInd < NUM_LEGS; legInd++)
    {

        // btScalar elapsedTime = m_Time - m_footContact_ReleaseTime[legInd];
        btScalar elapsedTime = m_Time ;
        btScalar fTargetPercent = (int(elapsedTime / 1000) % int(m_fCyclePeriod)) / m_fCyclePeriod;
        m_percent[legInd] = fTargetPercent;
        for (int jointInd = 0; jointInd < 3; jointInd ++)
        {

            btScalar freq = m_cpgFreq[legInd*3 + jointInd];
            btScalar phase = m_cpgPhase[legInd*3 + jointInd];
            btScalar amp = m_cpgAmp[legInd*3 + jointInd];
            btScalar offset = m_cpgOffset[legInd*3 + jointInd];

            btHingeConstraint* hingeC = this->getJoints()[legInd*3 + jointInd];
            btScalar fCurAngle = hingeC->getHingeAngle();
            btScalar fTargetAngle = amp*cos(2*M_PI*freq*fTargetPercent + phase) + offset;

            // if ( ((jointInd == 1) || (jointInd == 2)) && (fTargetPercent >= 0.5) )
            if ( (jointInd == 1)  && (fTargetPercent >= 0.5) )
            {
                    fTargetAngle = - fTargetAngle;
            }

            // btScalar lowLimit = m_minTarget[legInd*3+jointInd];
            // btScalar upLimit = m_maxTarget[legInd*3+jointInd];
            btScalar lowLimit = m_minAngle[legInd*3+jointInd];
            btScalar upLimit = m_maxAngle[legInd*3+jointInd];

            btScalar fTargetLimitAngle = hingeC->getLowerLimit() + (fTargetAngle + 1) * (hingeC->getUpperLimit() - hingeC->getLowerLimit()) / 2;
            //btScalar fTargetLimitAngle = lowLimit + (fTargetAngle + 1) * (upLimit - lowLimit) /2;

            btScalar fAngleError  = fTargetLimitAngle - fCurAngle;
            btScalar fDesiredAngularVel = fAngleError/timeStep;
//            if (fDesiredAngularVel > 1000)
//                fDesiredAngularVel = 1000;
//            else if (fDesiredAngularVel < -1000)
//                fDesiredAngularVel = -1000;

            m_angleVel[legInd*3+jointInd] = fDesiredAngularVel;
            hingeC->enableAngularMotor(true, fDesiredAngularVel, m_fMotorStrength);
        }
    }

    btVector3 comPos = m_bodies[0]->getCenterOfMassPosition();
    positionHistory.push_back(comPos);

    // yaw: z; pitch; y; roll: x
    btScalar yaw, pitch, roll;
    m_bodies[0]->getCenterOfMassTransform().getBasis().getEulerZYX(yaw, pitch, roll);
    btVector3 comRot(yaw, pitch, roll);
    rotationHistory.push_back(comRot);

    vector<btVector3> angles_one_frame;
    angles_one_frame = readAllJointAngles();
    angleHistory.push_back(angles_one_frame);

    vector<bool> contact_one_frame;
    readAllFootStatus(contact_one_frame);
    contactHistory.push_back(contact_one_frame);
}

void ant::checkFootContact(btScalar m_Time)
{
    int numManifolds = m_ownerWorld->getDispatcher()->getNumManifolds();
    for (int indMani = 0; indMani < numManifolds; indMani++)
    {
        btPersistentManifold* contactManifold =  m_ownerWorld->getDispatcher()->getManifoldByIndexInternal(indMani);
        const btCollisionObject* obA = contactManifold->getBody0();
        const btCollisionObject* obB = contactManifold->getBody1();

        btRigidBody* rb_a = static_cast<btRigidBody*>(const_cast<btCollisionObject*>(obA));
        btRigidBody* rb_b = static_cast<btRigidBody*>(const_cast<btCollisionObject*>(obB));

        int indLeg = checkWhichLeg(rb_b);
        if ((indLeg > 5) || (indLeg < 0))
            continue;
        int numContacts = contactManifold->getNumContacts();

        btPoint2PointConstraint* p2pConstraint;
        if (( numContacts!= 0) && (m_footStatus[indLeg] == false) && ((m_Time - m_footContact_ReleaseTime[indLeg]) > 750000) )
        {
            btManifoldPoint mp = contactManifold->getContactPoint(0);
            btVector3 local_a = mp.m_localPointA;
            btVector3 local_b = mp.m_localPointB;
            if (checkIsFootEnd(local_b) == false)
                continue;
            //p2pConstraint = new btPoint2PointConstraint(*rb_a, *rb_b, local_a, local_b);
            // btVector3 worldA = mp.getPositionWorldOnA();
            // DrawCircle(worldA[0], worldA[2]);

            // seems that 0.01 is a good option
            //p2pConstraint->setBreakingImpulseThreshold(0.025);
            //p2pConstraint->enableFeedback(true);
            //m_ownerWorld->addConstraint(p2pConstraint, true);
            //m_footContact[indLeg] = p2pConstraint;

            //m_ownerWorld->getDispatcher()->clearManifold(contactManifold);
            m_footStatus[indLeg] = true;

            // update target angle for this leg
            updateTargetAngle(indLeg);

            m_footContact_ReleaseTime[indLeg] = m_Time;
        }

    }

}

void ant::checkFootRelease(btScalar m_Time)
{
    // int numContact = m_footContact.size();
    for (int indLeg=0; indLeg < NUM_LEGS; indLeg++)
    {
        if (m_footStatus[indLeg] == false)
            continue;
        btScalar prob_timing = checkTiming(m_Time, indLeg);
        btScalar prob_neigh = checkNeighbor(indLeg);
        btScalar prob_extreme = checkExtremePose(indLeg, m_Time, m_footContact_ReleaseTime[indLeg]);
        btScalar prob = 1 - (1-prob_timing)*(1-prob_neigh)*(1-prob_extreme);
        if (prob > 0.95)
        {
            //btPoint2PointConstraint* footContact = m_footContact[indLeg];
            //if (footContact == NULL)
            //    continue;
            m_footStatus[indLeg] = false;
            //m_ownerWorld->removeConstraint(footContact);
            //m_footContact[indLeg] = NULL;
//            btScalar newpercent = 1 - m_percent[indLeg];
//            btScalar deltaPercent;
//            if (newpercent >= 0.5)
//                deltaPercent = newpercent - 0.5;
//            else
//                deltaPercent = newpercent;
//
//            m_footContact_ReleaseTime[indLeg] = m_Time - deltaPercent*m_fCyclePeriod*1000;
            m_footContact_ReleaseTime[indLeg] = m_Time;

            // update target angle for this leg
            updateTargetAngle(indLeg);
        }
    }

}

void ant::updateTargetAngle(int indLeg)
{
    for (int indJoint = 0; indJoint < 3; indJoint++)
    {
        if (m_angleVel[indLeg*3 + indJoint] > 0)
        {
            m_maxAngle[indLeg*3 + indJoint] = m_joints[indLeg*3 + indJoint]->getHingeAngle();
            m_minAngle[indLeg*3 + indJoint] = m_joints[indLeg*3 + indJoint]->getLowerLimit();
        }
        else
        {
            m_maxAngle[indLeg*3 + indJoint] = m_joints[indLeg*3 + indJoint]->getUpperLimit();
            m_minAngle[indLeg*3 + indJoint] = m_joints[indLeg*3 + indJoint]->getHingeAngle();
        }
    }
}

int ant::checkWhichLeg(btRigidBody* tibia)
{
    for (int indLeg = 0; indLeg < NUM_LEGS; indLeg++)
    {
        if (m_bodies[indLeg*3+3] == tibia)
            return indLeg;
    }
}

bool ant::checkIsFootEnd(btVector3 localPos)
{
    btVector3 footPos(0, -m_bodylength[3]/2, 0);
    if ((localPos - footPos).length() < 0.1)
        return true;
    else
        return false;
}

btScalar ant::checkTiming(btScalar m_Time, int indLeg)
{
    btScalar stanceTime = (m_Time - m_footContact_ReleaseTime[indLeg])/1000;
    btScalar prob = 1 - exp(-(stanceTime/m_fCyclePeriod*4));
    return prob;
}

btScalar ant::checkNeighbor(int indLeg)
{
    int direction = getWalkingDirection();
    int neighID;
    if (direction == 1)
    {
        if ((indLeg == 2) || (indLeg == 5))
            return 0;
        neighID = indLeg + 1;
    }
    else if (direction  == -1)
    {
        if ((indLeg == 0) || (indLeg == 3))
            return 0;
        neighID = indLeg - 1;
    }

    btVector3 curPos = getFootEndInGlobal(indLeg);
    btVector3 neighPos = getFootEndInGlobal(neighID);
    btScalar dist = (curPos - neighPos).length();
    btScalar prob = exp(-dist/0.2);
    return prob;
}

btScalar ant::checkExtremePose(int indLeg, btScalar currentTime, btScalar contactTime)
{
    if ((currentTime - contactTime) < m_fCyclePeriod*1000/2)
        return 0;
    btScalar angle_TC = m_joints[indLeg*3]->getHingeAngle();
    btScalar angle_CF = m_joints[indLeg*3+1]->getHingeAngle();
    btScalar angle_FT = m_joints[indLeg*3+2]->getHingeAngle();

    btScalar max_angle_TC = m_maxAngle[indLeg*3];
    btScalar min_angle_TC = m_minAngle[indLeg*3];
    btScalar max_angle_CF = m_maxAngle[indLeg*3+1];
    btScalar min_angle_CF = m_minAngle[indLeg*3+1];
    btScalar max_angle_FT = m_maxAngle[indLeg*3+2];
    btScalar min_angle_FT = m_minAngle[indLeg*3+2];

    btScalar prob_TC = (angle_TC - (max_angle_TC + min_angle_TC)/2)*(angle_TC - (max_angle_TC + min_angle_TC)/2)*4/(max_angle_TC - min_angle_TC) /(max_angle_TC - min_angle_TC);
    btScalar prob_CF = (angle_CF - (max_angle_CF + min_angle_CF)/2)*(angle_CF - (max_angle_CF + min_angle_CF)/2)*4/(max_angle_CF - min_angle_CF) /(max_angle_CF - min_angle_CF);
    btScalar prob_FT = (angle_FT - (max_angle_FT + min_angle_FT)/2)*(angle_FT - (max_angle_FT + min_angle_FT)/2)*4/(max_angle_FT - min_angle_FT) /(max_angle_FT - min_angle_FT);

    btScalar prob = 1 - (1-prob_TC)*(1-prob_CF)*(1-prob_FT);
    return prob;
}

int ant::getWalkingDirection()
{
    btVector3 headVec = head_body->getCenterOfMassPosition() - m_bodies[0]->getCenterOfMassPosition();
    btVector3 bodyVel = m_bodies[0]->getLinearVelocity();
    btScalar det = headVec.dot(bodyVel)/headVec.length()/bodyVel.length();
    if (det > 0.5)
        return 1;
    if (det < -0.5)
        return -1;
    // if it is walking in a side fashion. then do the cross product of these two factors, and compare the result vector with the up vector
    return 0;
}

btVector3 ant::getFootEndInGlobal(int indLeg)
{
    btTransform footInTibia;
    footInTibia.setIdentity();
    footInTibia.setOrigin(btVector3(0, -m_bodylength[3]/2, 0));
    btTransform trTibia = m_bodies[indLeg*3 + 3]->getCenterOfMassTransform();
    btTransform footEndInGlobal = trTibia*footInTibia;
    return footEndInGlobal.getOrigin();
}

void ant::resetTPose()
{

}


btHingeConstraint** ant::getJoints()
{
    return &m_joints[0];
}

neuron** ant::getNeurons()
{
    return &m_neurons[0];
}

muscle** ant::getMuscles()
{
    return &m_muscles[0];
}

btRigidBody** ant::getBodies()
{
    return &m_bodies[0];
}


btVector3 ant::getCOMposition()
{
    return m_bodies[0]->getCenterOfMassPosition();
}

void ant::readAllFootStatus(vector<bool>& contact_per_frame)
{
    for(int indLeg = 0; indLeg < NUM_LEGS; indLeg++)
    {
        if (m_footStatus[indLeg] == true)
            contact_per_frame.push_back(1);
        else
            contact_per_frame.push_back(0);
    }
}

void ant::setAngleLimits(btScalar max_Angle[], btScalar min_Angle[])
{
    // std::copy(std::begin(max_Angle), std::end(max_Angle), std::begin(m_maxAngle));
    // std::copy(std::begin(min_Angle), std::end(min_Angle), std::begin(m_minAngle));
    memcpy(m_maxAngle, max_Angle, NUM_JOINT);
    memcpy(m_minAngle, min_Angle, NUM_JOINT);
}

void ant::drawDebugFrame()
{
    btTransform localB, worldB;

    for (int i=0; i<6; i++)
    {
        /*
                btGeneric6DofConstraint* HIP = static_cast<btGeneric6DofConstraint*>(this->getJoints()[i*2]);
                localB = HIP->getFrameOffsetB();
                worldB = HIP->getRigidBodyB().getWorldTransform();
                btTransform HipFrame = worldB*localB;
                drawFrame(HipFrame);
        */
        btHingeConstraint* knee = static_cast<btHingeConstraint*>(this->getJoints()[i*2+1]);
        localB = knee->getBFrame();
        worldB = knee->getRigidBodyB().getWorldTransform();
        btTransform KneeFrame = worldB*localB;
        drawFrame(KneeFrame);
    }
    btTransform orig;
    orig.setIdentity();
    drawFrame(orig);
}
