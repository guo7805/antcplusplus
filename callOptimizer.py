import optimizer
import flut
import time
import numpy as np

#nadirUtopia = [[3040, 4525, 392], [81, 1, 3]]
#weightList = [0.5, 0.25, 0.25]
#start_time = time.time()
#optimizer.optimizeByLimits(weightList=weightList, nadirUtopia=nadirUtopia)
#optimizer.optimizeByCMA(useMP=True, weightArray=weightList, nadirUtopia=nadirUtopia)
#print("--- %s seconds ---" % (time.time() - start_time))
#nadirUtopia = [[3040, 4525, 392], [81, 1, 3]]
#weightList = []

#for w_m in np.linspace(0, 1, 11):
    #for w_t in np.linspace(0, w_m, w_m/0.1+1):
        #weightList.append([w_m, w_t])

#fitArray = []

#for weightArray in weightList:
    #fit = optimizer.optimizeByCMA(useMP=True, weightArray=weightArray, nadirUtopia=nadirUtopia)
    #fitArray.append(weightArray+[fit])
#print("--- %s seconds ---" % (time.time() - start_time))


#objSelList = ['dev', 'rot', 'vel']
#xList = np.loadtxt('utopia.dat')
#resList = []
#indGen = 0
#indX = 1001

#for objSel in objSelList:
    #tempRes = []
    #for x in xList:
        #fit = optimizer.realFitnessFun(x, indGen, indX, objSel=objSel)
        #tempRes.append(fit)
    #resList.append(tempRes)

#print('finished')

#optimizer.getNadirUtopiaPoints()

def generateflut(weightArray=None, nadirUtopia=None):
    flut.generateFlut(zActive=False, weightArray=weightArray, nadirUtopia=nadirUtopia)

generateflut()