#ifndef NEURON_H
#define NEURON_H
#include "btBulletDynamicsCommon.h"
#include <fstream>
using namespace std;

class neuron
{
    btScalar v;
    btScalar u;

    btScalar params[4];
    int index;
    ofstream record;
    btScalar dutyfactor;
    public:
        neuron(int ind, btScalar df);
        virtual ~neuron();
        btScalar activeS(btScalar deltaT, btScalar percent);
        btScalar restS();
        btScalar getu();
        btScalar getv();
    protected:
    private:
};

#endif // NEURON_H
