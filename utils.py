import glob, os, shutil, csv

def readDLM(fname, startR, numberR, startC, numberC):
    result = []
    with open(fname) as f:
        reader = csv.reader(f)
        indR = 0
        for row in reader:
            indR = indR + 1            
            if (indR < startR): 
                continue            
            indC = 0
            line = row[0].split()[startC : startC + numberC]
            
            if (numberR != 0) and (indR >= startR + numberR):
                break            
            
        if not result:
            result = line
        else:
            result = [result, line]       
                
    return result


def cleanup():
    # Move the output from the cma optimizer to the subfolder record/optimizer/
    # Clean the files from last optimization
    files = glob.glob("record/optimizer/*.dat")
    for file in files:
        os.remove(file)
    # Move the new log files
    files = glob.iglob(os.path.join(".", "*.dat"))
    for file in files:
        if os.path.isfile(file):
            shutil.move(file, "record/optimizer/")