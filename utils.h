#ifndef UTILS_H
#define UTILS_H


void vertex(btVector3 &v)
{
    glVertex3d(v.getX(), v.getY(), v.getZ());
}

void drawFrame(btTransform &tr)
{
    const float fSize = 1.f;

    glLineWidth( 3.0 );

    glBegin(GL_LINES);

    // x
    glColor3f(255.f,0,0);
    btVector3 vX = tr*btVector3(fSize,0,0);
    vertex(tr.getOrigin());
    vertex(vX);

    // y
    glColor3f(0,255.f,0);
    btVector3 vY = tr*btVector3(0,fSize,0);
    vertex(tr.getOrigin());
    vertex(vY);

    // z
    glColor3f(0,0,255.f);
    btVector3 vZ = tr*btVector3(0,0,fSize);
    vertex(tr.getOrigin());
    vertex(vZ);

    glEnd();
}

btVector3 getEulerYZ(btMatrix3x3& tr)
{

    btVector3 row0 = tr.getRow(0);
    btVector3 row1 = tr.getRow(1);
    btVector3 angles(btAsin(row0[2]), btAsin(row1[0]), btScalar(0.0));

    return angles;
}

void DrawCircle(float cx, float cy, float radius = 0.5, int num_segments = 20)
{
    float angle, x2, y2, x1, y1;
    x1 = cx;
    x2 = cy;
    glColor3f(1.0,1.0,0.6);

    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(cx,cy);

    for (angle=1.0f; angle<361.0f; angle+=0.2)
    {
        x2 = x1+sin(angle)*radius;
        y2 = y1+cos(angle)*radius;
        glVertex2f(x2,y2);
    }

    glEnd();
}


#endif
