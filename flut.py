import numpy as np
import scipy as sp
import os

def generateFlut(zActive, weightArray, nadirUtopia):
    os.makedirs('flut', exist_ok=True)
    xInit = initX()
    
    coords = generateCube(zActive=zActive)
    # 1 second
    deltaT = 1
    for coord in coords:
        optArgs = (coord, weightArray, nadirUtopia)
        xopt, fopt, iters, funcalls, warnflag, allvecs = sp.optimize.fmin(optimalFun, xInit, optArgs, full_output=True, disp=True, retall=True)

def initX():
    paramList = []
    with open('param/CPG_Controller.cfg', 'r') as fileHandle:
        allLines = fileHandle.readlines()
        selLines = [allLines[1], allLines[3], allLines[5], allLines[7]]
        for line in selLines:
            line_list = [float(e) for e in line.split()]
            paramList = paramList + line_list
    return paramList

def generateCube(zActive=True):
    flutconfig = np.loadtxt('param/flut/flut.cfg',skiprows=1)
    max_x, min_x, step_x = flutconfig[0][:]
    max_y, min_y, step_y = flutconfig[1][:]
    max_z, min_z, step_z = flutconfig[2][:]
    
    coord_x = np.linspace(min_x, max_x, step_x)
    coord_y = np.linspace(min_y, max_y, step_y)    
    coord_z = np.linspace(min_z, max_z, step_z)    
    coordList = []
    
    for x in coord_x:
        for y in coord_y:            
            if zActive:
                coord = []
                for z in coord_z:                    
                    coord.append([x, y, z])                        
            else:
                coord = [x, y]
            coordList.append(coord)
    return coordList
    
def optimalFun(x, *args):    
    saveParam(x, paramType='cpg')
    
    simulationBody()
    
    fitnessValue = computeObjValue(target=args[0], weightArray=args[1], nadirUtopia=args[2])
    
    return fitnessValue        

def saveParam(x, paramType):
    dim = len(x)
    cat = dim//18    
    fileStr = ''
    for indCat in range(cat):
        paramList = x[indCat*18:indCat*18 + 18]        
        paramStr = list2string(paramList)
        fileStr = fileStr + 'heading line\n' + paramStr + '\n'
    with open("flut/CPG_Controller.cfg", "w") as outFile:
        outFile.write(fileStr)
    


def simulationBody():
    # strMax = ' '.join([str(e) for e in max_Angle])
    # strMin = ' '.join([str(e) for e in min_Angle])
    cmd = 'bin/Flut/ant'
    os.system(cmd)    
    

def computeObjValue(target, scalar=True, objSel=None, weightArray=None, nadirUtopia=None):
    posArray = np.loadtxt("flut/position.txt")
    # rotArray = np.loadtxt("flut/rotation.txt")    
    

    
    if nadirUtopia == None:
        tarPen = targetError(target, posArray[-1, :])
        devPen = deviationError(posArray)
        velPen = velocityError(target, posArray)
    else:
        tarPen = targetError(target, posArray[-1, :], normalize=True, nadir=nadirUtopia[0][0], utopia=nadirUtopia[1][0])
        velPen = velocityError(target, posArray, normalize=True, nadir=nadirUtopia[0][1], utopia=nadirUtopia[1][1])        
        devPen = deviationError(posArray, normalize=True, nadir=nadirUtopia[0][2], utopia=nadirUtopia[1][2])

    
    if objSel is not None:
        if objSel == 'tar':
            return tarPen
        elif objSel == 'dev':
            return devPen
        elif objSel == 'vel':
            return velPen
    
        
    
    # calculate the fitness
    if scalar == True:
        if weightArray is not None:
            w = [weightArray[0], weightArray[1], 1-weightArray[0]-weightArray[1]]
        else:
            w = [1/3, 1/3, 1/3]       
        fitness = sum(map(lambda x, y: x*y, [tarPen, devPen, velPen], w))        
        print(fitness)
    else:
        fitness = [tarPen, devPen, velPen]
    return fitness

def targetError(target, result, normalize=False, nadir=None, utopia=None):
    if len(target) == 2:
        result = result[0:2]
    errorSum = (target - result).dot(target - result)
    if normalize == True:
        errorSum = (diff - utopia)/(nadir - utopia)
    return errorSum

def deviationError(posArray, normalize=False, nadir=None, utopia=None):
    # constraining the lateral and vertical deviations of COM
    # xError = 0.0    
    yError = 0.0
    zError = 0.0
    yPosArray = posArray[:,1]
    zPosArray = posArray[:,2]
    
    targetYPos = 0.45
    targetZPos = 0.0
    
    yError = (yPosArray - targetYPos).T * (yPosArray - targetYPos)
    zError = (zPosArray - targetZPos).T * (zPosArray - targetZPos)
    errorSum = sum(yError) + sum(zError)
    if normalize == True:
        errorSum = (errorSum - utopia)/(nadir - utopia)
    return errorSum    
    

def velocityError(target, posArray, normalize=False, nadir=None, utopia=None):
    posDiff = np.diff(posArray, axis=0)
    errorSum = 0
    for vec in posDiff:
        if len(target) == 2:
            vec = vec[0:2]
        if (vec.dot(target) < 0):
            errorSum = errorSum + vec.dot(vec)
    if normalize == True:
        errorSum = (errorSum - utopia)/(nadir - utopia)
    return errorSum

def list2string(inList):
    outString = ' '.join( "%.6f" % e for e in inList)
    return outString